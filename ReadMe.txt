map load issue fix

// loading map
		$scope.getMap = function () {
			$scope.showSaveBtn = false;
			$ionicLoading.show({
				template: '<ion-spinner icon="ios" class="spinner-calm"></ion-spinner><p>Loading</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			cordova.plugins.diagnostic.getLocationAuthorizationStatus(function (status) {
				console.log('getLocationAuthorizationStatus is ', status);
				switch (status) {
					case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
						console.log("Permission not requested");
						break;
					case cordova.plugins.diagnostic.permissionStatus.DENIED:
						console.log("Permission denied");
						cordova.plugins.diagnostic.switchToLocationSettings();
						$ionicLoading.hide();
						setTimeout(function () {
							$scope.closeModal(4);
						}, 2000); //smooth closing effect of modal
						break;
					case cordova.plugins.diagnostic.permissionStatus.GRANTED:
						console.log("Permission granted always");
						break;
					case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
					// case cordova.plugins.diagnostic.permissionStatus.NOT_DETERMINED:
						$scope.requestMap();
						break;
				}
				if(status == 'not_determined'){
					cordova.plugins.diagnostic.requestLocationAuthorization(function (status) {
						console.log('requestLocationAuthorization status is ', status);
						switch (status) {
							case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
							case cordova.plugins.diagnostic.permissionStatus.GRANTED:
							case cordova.plugins.diagnostic.permissionStatus.DENIED:
							case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
							case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
								console.log("Permission granted to use the LOCATION");
								$scope.requestMap();
							break;
						}
					}, function (error) {
						console.error("The following error occurred: " + error);
					}, cordova.plugins.diagnostic.locationAuthorizationMode.GRANTED_WHEN_IN_USE);
				}
			}, function (error) {
				console.error("The following error occurred: " + error);
			});

			$scope.requestMap = function(){
				cordova.plugins.diagnostic.isLocationAvailable(function (available) {
					console.log("Location is " + (available ? "available" : "not available"));
					if (!available) {
						cordova.plugins.diagnostic.switchToLocationSettings();
						$ionicLoading.hide();
						setTimeout(function () {
							$scope.closeModal(4);
						}, 2000); //smooth closing effect of modal
					} else {

						$scope.locationCtrl = {
							coordinates: null
						};
						var options = { timeout: 10000, enableHighAccuracy: true };

						$cordovaGeolocation.getCurrentPosition(options).then(function (position) {

							// geoCoder
							var geocoder = new google.maps.Geocoder();
							var latLng;
							var lat;
							var lng;

							function geocodePosition(pos) {
								document.getElementById('mapAdd').innerHTML = '';
								geocoder.geocode({
									latLng: pos
								}, function (responses) {
									if (responses && responses.length > 0) {
										console.log('inside the geocode response ', responses);
										// document.getElementById('mapPointerAdd').innerHTML = responses[0].formatted_address; //mapAdd
										document.getElementById('mapAdd').innerHTML = responses[0].formatted_address;
										$scope.address.locality = responses[0].formatted_address;
										if (responses[0].formatted_address) {
											$scope.showSaveBtn = true;
											$scope.$apply();
										} else {
											$scope.showSaveBtn = false;
										}
									} else {
										// document.getElementById('mapPointerAdd').innerHTML = 'Cannot determine address at this location.';
										//$scope.address.locality = 'Cannot determine address at this location.';
										console.log('can not get the address');
									}
								});
							}
							// check for update Address
							if (Address.getUpdateFlag()) {
								console.log('updateAddress get map lat ' + $scope.user.currentAddress.lat + 'get map lng ' + $scope.user.currentAddress.lng);
								//for users who's lat lng is NULL show the current address
								if (!$scope.user.currentAddress.lat || !$scope.user.currentAddress.lng) {
									latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
									$scope.address.lat = position.coords.latitude;
									$scope.address.lng = position.coords.longitude;
									console.log('lat is ' + $scope.address.lat + ' lng is ' + $scope.address.lng);
									geocodePosition(latLng);
								} else {
									latLng = new google.maps.LatLng(+$scope.user.currentAddress.lat, +$scope.user.currentAddress.lng);
									geocodePosition(latLng);
								}
							} else { //current address
								latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
								$scope.address.lat = position.coords.latitude;
								$scope.address.lng = position.coords.longitude;
								console.log('lat is ' + $scope.address.lat + ' lng is ' + $scope.address.lng);
								geocodePosition(latLng);
							}

							var mapOptions = {
								center: latLng,
								zoom: 17,
								mapTypeId: google.maps.MapTypeId.ROADMAP,
								disableDefaultUI: true,
								mapTypeControl: false,
								streetViewControl: false,
								draggable: true,
								//gestureHandling: 'cooperative'
							};

							$scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
							$scope.locationCtrl.coordinates = $scope.map.getCenter().toUrlValue();
							console.log('the cordinates are ', $scope.locationCtrl);
							var GeoMarker = new GeolocationMarker($scope.map);
							var handleMapEvent = function () {
								$ionicLoading.show();
								$scope.locationCtrl.coordinates = $scope.map.getCenter().toUrlValue();
								// $scope.$apply();
								console.log('the cordinates are ', $scope.locationCtrl);
								$scope.address.lat = +$scope.locationCtrl.coordinates.split(',')[0];
								$scope.address.lng = +$scope.locationCtrl.coordinates.split(',')[1];
								latLng = new google.maps.LatLng($scope.address.lat, $scope.address.lng);
								geocodePosition(latLng);
								$scope.$apply();
								$ionicLoading.hide();
							};
							// google.maps.event.addListener($scope.map, "dragend", function () {
							//  console.log('center_changed event triggered');
							//  handleMapEvent();
							// });
							// google.maps.event.addListener($scope.map, "center_changed", function () {
							//  console.log('center_changed event triggered');
							//  handleMapEvent();
							// });
							google.maps.event.addListener($scope.map, 'idle', function () {
								// do something only the first time the map is loaded
								handleMapEvent();
							});

						}, function (error) {
							console.log("Could not get location. Make sure your GPS is ON");
						});
					}
				}, function (error) {
					console.error("The following error occurred: " + error);
				});
			};
		}; //end of getMap()



//Below function is called twice
		// $scope.generateWalletProducts = function(){
		// 	$scope.walletProducts = $scope.product.map(function(obj){
		// 		obj.vendors = $scope.vendors;
		// 		return obj;
		// 	});
		// 	console.log('walletProducts with vendor', $scope.walletProducts);
		// };

		// products stub data
		// $scope.product = [{ name: 'Add One Bottle to Wallet.', id: 1, money: 40, offer: 'Add A Bottle' }, { name: 'Add 3 Bottles to Wallet and get 1 FREE Bottle.', id: 2, money: 120, offer: 'Silver Offer' }, { name: 'Add 5 Bottles to Wallet and get 2 FREE Bottles.', id: 3, money: 200, offer: 'Gold Offer' }, { name: 'Add 7 Bottles to Wallet and get 3 FREE Bottles.', id: 4, money: 280, offer: 'Platinum Offer' }];

		// slots stub data
		// $scope.slots = [{ id: 1, time: '9AM-10AM' }, { id: 2, time: '10AM-11AM' }, { id: 3, time: '11AM-12PM' }, { id: 4, time: '12PM-1PM' }, { id: 5, time: '3PM-4PM' }, { id: 6, time: '4PM-5PM' }, { id: 7, time: '5PM-6PM' }, { id: 8, time: '6PM-7PM' }, { id: 9, time: '7PM-8PM' }, { id: 10, time: '8PM-9PM' }];

		// vendors stub data
		// $scope.vendors = [{ id: 1, name: 'Pearl Drops', image: 'img/nandu.png', price: 40 }, { id: 2, name: 'Bisleri', image: 'img/nandu.png', price: 80 }];

				// $scope.walletProducts.map(function(obj){
				// 	obj.money = obj.noOfBottles * $scope.selectedVendor.price;
				// 	obj.save = obj.offer.freeBottles * $scope.selectedVendor.price;
				// 	return obj;
				// });
				// console.log('walletProducts with money', $scope.walletProducts);

cordova plugin add https://gitlab.com/visikolkar/paytm-plugin.git --variable GENERATE_URL="http://www.eazybottle.jvmhost.net/EazyBottle/app/paytm/generateChecksum" --variable VERIFY_URL="http://www.eazybottle.jvmhost.net/EazyBottle/app/paytm/callback" --variable MERCHANT_ID="EazyBo06509875098816" --variable INDUSTRY_TYPE_ID="Retail" --variable WEBSITE="APP_STAGING"
cordova plugin add https://gitlab.com/visikolkar/paytm-plugin.git --variable GENERATE_URL="https://eazybottle.com/EazyBottle/app/paytm/generateChecksum" --variable VERIFY_URL="https://eazybottle.com/EazyBottle/app/paytm/callback" --variable MERCHANT_ID="EazyBo80812090291130" --variable INDUSTRY_TYPE_ID="Retail109" --variable WEBSITE="EazyWEB"