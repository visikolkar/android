@ECHO OFF
rem Ensure this Node.js and npm are first in the PATH
set "PATH=%APPDATA%\npm;%~dp0;%PATH%"

setlocal enabledelayedexpansion
pushd "%~dp0"

rem Figure out the Node.js version.
set print_version=node.exe -p -e "process.versions.node + ' (' + process.arch + ')'"
for /F "usebackq delims=" %%v in (`%print_version%`) do set version=%%v

rem Print message.
if exist npm.cmd (
  echo Your environment has been set up for using Node.js !version! and npm.
) else (
  echo Your environment has been set up for using Node.js !version!.
)

popd
endlocal

rem If we're in the Node.js directory, change to the user's home dir.
if "%CD%\"=="%~dp0" cd /d "%HOMEDRIVE%%HOMEPATH%"
@ECHO ON
setlocal
cd /d %~dp0
cmd /C cordova ionic -v || npm install -g cordova ionic
cmd /C cordova platform add android@latest
cmd /C cordova plugin add cordova-plugin-app-version
cmd /C cordova plugin add cordova-plugin-compat
cmd /C cordova plugin add cordova-plugin-device
cmd /C cordova plugin add cordova-plugin-geolocation
cmd /C cordova plugin add cordova-plugin-inappbrowser
cmd /C cordova plugin add cordova-plugin-nativeclicksound
cmd /C cordova plugin add cordova-plugin-network-information
cmd /C cordova plugin add cordova-plugin-splashscreen
cmd /C cordova plugin add cordova-plugin-statusbar
cmd /C cordova plugin add cordova-plugin-whitelist
cmd /C cordova plugin add cordova-plugin-x-toast
cmd /C cordova plugin add cordova.plugins.diagnostic
cmd /C cordova plugin add ionic-plugin-deploy
cmd /C cordova prepare
