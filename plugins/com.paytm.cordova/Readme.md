Installation
============

```
cordova plugin add https://gitlab.com/visikolkar/paytm-plugin.git --variable GENERATE_URL="https://eazybottle.com/EazyBottle/app/paytm/generateChecksum" --variable VERIFY_URL="https://eazybottle.com/EazyBottle/app/paytm/callback" --variable MERCHANT_ID="EazyBo80812090291130" --variable INDUSTRY_TYPE_ID="Retail109" --variable WEBSITE="EazyWEB"

cordova plugin add https://gitlab.com/visikolkar/paytm-plugin.git --variable GENERATE_URL="http://www.eazybottle.jvmhost.net/EazyBottle/app/paytm/generateChecksum" --variable VERIFY_URL="http://www.eazybottle.jvmhost.net/EazyBottle/app/paytm/callback" --variable MERCHANT_ID="EazyBo06509875098816" --variable INDUSTRY_TYPE_ID="Retail" --variable WEBSITE="APP_STAGING"
```

Usage
=====

```
window.plugins.paytm.startPayment(txn_id, customer_id, email, phone, amount, successCallback, failureCallback);
```