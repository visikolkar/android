#import "PaymentsSDK.h"
#import <Cordova/CDV.h>

@interface PayTMCordova : CDVPlugin <PGTransactionDelegate>

- (void)startPaymentiOS:(CDVInvokedUrlCommand*)command;

@end
