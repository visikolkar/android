#import "PayTMCordova.h"
#import <Cordova/CDV.h>

@implementation PayTMCordova{
    NSString* callbackId;
    PGTransactionViewController* txnController;
}


- (void)startPaymentiOS:(CDVInvokedUrlCommand *)command {
    
    callbackId = command.callbackId;
//    orderId, customerId, email, phone, amount,
    NSString *orderId  = [command.arguments objectAtIndex:0];
    NSString *customerId = [command.arguments objectAtIndex:1];
    NSString *email = [command.arguments objectAtIndex:2];
    NSString *phone = [command.arguments objectAtIndex:3];
    NSString *amount = [command.arguments objectAtIndex:4];
    NSString *checkSum = [command.arguments objectAtIndex:5];
    
    NSBundle* mainBundle;
    mainBundle = [NSBundle mainBundle];
    
    NSString* paytm_validate_url = [mainBundle objectForInfoDictionaryKey:@"PayTMVerifyChecksumURL"];
    NSString* paytm_merchant_id = [mainBundle objectForInfoDictionaryKey:@"PayTMMerchantID"];
    NSString* paytm_ind_type_id = [mainBundle objectForInfoDictionaryKey:@"PayTMIndustryTypeID"];
    NSString* paytm_website = [mainBundle objectForInfoDictionaryKey:@"PayTMWebsite"];
    
    //Step 1: Create a default merchant config object
    PGMerchantConfiguration *mc = [PGMerchantConfiguration defaultConfiguration];
    
    //Step 2: Create the order with whatever params you want to add. But make sure that you include the merchant mandatory params
    NSMutableDictionary *orderDict = [NSMutableDictionary new];
    //Merchant configuration in the order object
    orderDict[@"MID"] = paytm_merchant_id;
    orderDict[@"ORDER_ID"] = orderId;
    orderDict[@"CUST_ID"] = customerId;
    orderDict[@"PHONE"] = phone;
    orderDict[@"EMAIL"] = email;
    orderDict[@"THEME"] = @"merchant";
    orderDict[@"REQUEST_TYPE"] = @"DEFAULT";
    orderDict[@"INDUSTRY_TYPE_ID"] = paytm_ind_type_id;
    orderDict[@"CHANNEL_ID"] = @"WAP";
    orderDict[@"TXN_AMOUNT"] = amount;
    orderDict[@"WEBSITE"] = paytm_website;
    orderDict[@"CALLBACK_URL"] = [NSString stringWithFormat:paytm_validate_url,orderId];
    orderDict[@"CHECKSUMHASH"] = checkSum;
    
    PGOrder *order = [PGOrder orderWithParams:orderDict];
    DEBUGLOG(@"ViewController::order object is = %@",order);
    txnController = [[PGTransactionViewController alloc] initTransactionForOrder:order];
    txnController.serverType = eServerTypeProduction;
    txnController.merchant = mc;
    txnController.delegate = self;
    txnController.loggingEnabled = true;
    UIViewController *rootVC = [[[[UIApplication sharedApplication] delegate] window] rootViewController];
//    [rootVC.navigationController pushViewController:txnController animated:true];
    [rootVC presentViewController:txnController animated:YES completion:nil];
    // [self showController:txnController];
}

#pragma mark PGTransactionViewController delegate

-(void)didFinishedResponse:(PGTransactionViewController *)controller response:(NSDictionary *)responseString {
    DEBUGLOG(@"ViewController::didFinishedResponse:response = %@", responseString);
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_OK messageAsDictionary:responseString];
    [result setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    [txnController dismissViewControllerAnimated:YES completion:nil];
}

- (void)didCancelTransaction:(PGTransactionViewController *)controller{

    //DEBUGLOG(@"ViewController::didCancelTransaction error = %@ response= %@", error, response);
//    NSString *msg = nil;
//    if (!error) msg = [NSString stringWithFormat:@"Successful"];
//    else msg = [NSString stringWithFormat:@"UnSuccessful"];
//    
//    [[[UIAlertView alloc] initWithTitle:@"Transaction Cancel" message:msg delegate:nil cancelButtonTitle:@"OK" otherButtonTitles:nil] show];
//    [self removeController:controller];
//    
    CDVPluginResult* result = [CDVPluginResult resultWithStatus:CDVCommandStatus_ERROR messageAsDictionary:nil];
    [result setKeepCallbackAsBool:YES];
    [self.commandDelegate sendPluginResult:result callbackId:callbackId];
    [txnController dismissViewControllerAnimated:YES completion:nil];

}

-(void)errorMisssingParameter:(PGTransactionViewController *)controller error:(NSError *) error{
    
    DEBUGLOG(@"ViewController::errorMisssingParameter error = %@",error);
}

@end