angular.module('eazybottle', ['ionic', 'ngCordova', 'ionic.cloud', 'ionic.ion.imageCacheFactory'])

	.run(function ($ionicPlatform, $rootScope, $cordovaNetwork, $window, $ionicHistory, $state) {
		$ionicPlatform.ready(function () {
			//$window.localStorage.clear();
			$ionicHistory.clearCache();
			$ionicHistory.clearHistory();
			// Hide the accessory bar by default (remove this to show the accessory bar above the keyboard
			// for form inputs)
			if (window.cordova && window.cordova.plugins && window.cordova.plugins.Keyboard) {
				cordova.plugins.Keyboard.hideKeyboardAccessoryBar(true);
				cordova.plugins.Keyboard.disableScroll(true);

			}
			if (window.StatusBar) {
				// org.apache.cordova.statusbar required
				StatusBar.styleDefault();
			}
			//native click sound using plugin
			var clickyClasses = ['sound-click', 'button'];
			nativeclick.watch(clickyClasses);

			// Check for network connection
			var type = $cordovaNetwork.getNetwork();
			var isOnline = $cordovaNetwork.isOnline();
			var isOffline = $cordovaNetwork.isOffline();
			// listen for Online event
			$rootScope.$on('$cordovaNetwork:online', function (event, networkState) {
				var onlineState = networkState;
				console.log('ONLINE STATE ', networkState);
				$rootScope.showCard = false;
			});

			// listen for Offline event
			$rootScope.$on('$cordovaNetwork:offline', function (event, networkState) {
				var offlineState = networkState;
				console.log('OFFLINE STATE ', networkState);
				$rootScope.showCard = true;
			});
			$ionicPlatform.registerBackButtonAction(function () {
				if ($state.current.name === "tab.dash") {
					if ($rootScope.backButtonPressedOnceToExit) {
						ionic.Platform.exitApp();
					}
					else {
						$rootScope.backButtonPressedOnceToExit = true;
						window.plugins.toast.showShortBottom(
							"Double tap to exit", function (a) { }, function (b) { }
						);
						setTimeout(function () {
							$rootScope.backButtonPressedOnceToExit = false;
						}, 2000);
					}
				} else {
					$ionicHistory.goBack();
				}
			}, 100);

			//polyfills for 4.4.4 versions and earlier
			if (!String.prototype.startsWith) {
				String.prototype.startsWith = function(searchString, position){
				  return this.substr(position || 0, searchString.length) === searchString;
			  };
			}
			
			if (!Array.prototype.find) {
				Object.defineProperty(Array.prototype, 'find', {
					value: function (predicate) {
						'use strict';
						if (this == null) {
							throw new TypeError('Array.prototype.find called on null or undefined');
						}
						if (typeof predicate !== 'function') {
							throw new TypeError('predicate must be a function');
						}
						var list = Object(this);
						var length = list.length >>> 0;
						var thisArg = arguments[1];
						var value;

						for (var i = 0; i < length; i++) {
							value = list[i];
							if (predicate.call(thisArg, value, i, list)) {
								return value;
							}
						}
						return undefined;
					}
				});
			} //for Android versions 4.4.4 earlier 
			if (!Array.prototype.findIndex) {
				Object.defineProperty(Array.prototype, 'findIndex', {
					value: function (predicate) {
						'use strict';
						if (this == null) {
							throw new TypeError('Array.prototype.findIndex called on null or undefined');
						}
						if (typeof predicate !== 'function') {
							throw new TypeError('predicate must be a function');
						}
						var list = Object(this);
						var length = list.length >>> 0;
						var thisArg = arguments[1];
						var value;

						for (var i = 0; i < length; i++) {
							value = list[i];
							if (predicate.call(thisArg, value, i, list)) {
								return i;
							}
						}
						return -1;
					},
					enumerable: false,
					configurable: false,
					writable: false
				});
			} //for Android versions 4.4.4 earlier
			if (!Array.prototype.filter) {
				Array.prototype.filter = function (fun/*, thisArg*/) {
					'use strict';

					if (this === void 0 || this === null) {
						throw new TypeError();
					}

					var t = Object(this);
					var len = t.length >>> 0;
					if (typeof fun !== 'function') {
						throw new TypeError();
					}

					var res = [];
					var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
					for (var i = 0; i < len; i++) {
						if (i in t) {
							var val = t[i];

							// NOTE: Technically this should Object.defineProperty at
							//       the next index, as push can be affected by
							//       properties on Object.prototype and Array.prototype.
							//       But that method's new, and collisions should be
							//       rare, so use the more-compatible alternative.
							if (fun.call(thisArg, val, i, t)) {
								res.push(val);
							}
						}
					}

					return res;
				};
			}//4.4.4 versions earlier
			if (!Array.prototype.some) {
				Array.prototype.some = function(fun/*, thisArg*/) {
				  'use strict';
			  
				  if (this == null) {
					throw new TypeError('Array.prototype.some called on null or undefined');
				  }
			  
				  if (typeof fun !== 'function') {
					throw new TypeError();
				  }
			  
				  var t = Object(this);
				  var len = t.length >>> 0;
			  
				  var thisArg = arguments.length >= 2 ? arguments[1] : void 0;
				  for (var i = 0; i < len; i++) {
					if (i in t && fun.call(thisArg, t[i], i, t)) {
					  return true;
					}
				  }
			  
				  return false;
				};
			}
			if (!Array.prototype.reduce) {
				Object.defineProperty(Array.prototype, 'reduce', {
				  value: function(callback /*, initialValue*/) {
					if (this === null) {
					  throw new TypeError( 'Array.prototype.reduce ' + 
						'called on null or undefined' );
					}
					if (typeof callback !== 'function') {
					  throw new TypeError( callback +
						' is not a function');
					}
			  
					// 1. Let O be ? ToObject(this value).
					var o = Object(this);
			  
					// 2. Let len be ? ToLength(? Get(O, "length")).
					var len = o.length >>> 0; 
			  
					// Steps 3, 4, 5, 6, 7      
					var k = 0; 
					var value;
			  
					if (arguments.length >= 2) {
					  value = arguments[1];
					} else {
					  while (k < len && !(k in o)) {
						k++; 
					  }
			  
					  // 3. If len is 0 and initialValue is not present,
					  //    throw a TypeError exception.
					  if (k >= len) {
						throw new TypeError( 'Reduce of empty array ' +
						  'with no initial value' );
					  }
					  value = o[k++];
					}
			  
					// 8. Repeat, while k < len
					while (k < len) {
					  // a. Let Pk be ! ToString(k).
					  // b. Let kPresent be ? HasProperty(O, Pk).
					  // c. If kPresent is true, then
					  //    i.  Let kValue be ? Get(O, Pk).
					  //    ii. Let accumulator be ? Call(
					  //          callbackfn, undefined,
					  //          « accumulator, kValue, k, O »).
					  if (k in o) {
						value = callback(value, o[k], k, o);
					  }
			  
					  // d. Increase k by 1.      
					  k++;
					}
			  
					// 9. Return accumulator.
					return value;
				  }
				});
			}
		});

	})

	.config(function ($stateProvider, $urlRouterProvider, $ionicConfigProvider, $ionicCloudProvider) {

		$ionicCloudProvider.init({
			"core": {
				"app_id": "08b30036"
			}
		});

		$ionicConfigProvider.tabs.position('bottom');

		$ionicConfigProvider.scrolling.jsScrolling(false); //for Native Scroll laf

		$stateProvider

			.state('login', {
				url: '/login/',
				templateUrl: 'templates/login.html',
				controller: 'LoginCtrl'
			})

			// setup an abstract state for the tabs directive
			.state('tab', {
				url: '/tab',
				abstract: true,
				templateUrl: 'templates/tabs.html'
			})

			// Each tab has its own nav history stack:

			.state('tab.dash', {
				url: '/dash/',
				views: {
					'tab-dash': {
						templateUrl: 'templates/tab-dash.html',
						controller: 'DashCtrl',
					}
				},
				onEnter: function ($state, Auth) {
					if (!Auth.isLoggedIn()) {
						$state.go('login');
					} else {
						console.log('logged in already!')
					}
				}
			})

			.state('tab.orders', {
				url: '/orders',
				views: {
					'tab-orders': {
						templateUrl: 'templates/tab-orders.html',
						controller: 'OrdersCtrl'
					}
				}
			})
			.state('tab.order-detail', {
				url: '/orders/:orderId',
				views: {
					'tab-orders': {
						templateUrl: 'templates/order-detail.html',
						controller: 'OrderDetailCtrl'
					}
				}
			})

			.state('tab.account', {
				url: '/account',
				views: {
					'tab-account': {
						templateUrl: 'templates/tab-account.html',
						controller: 'AccountCtrl'
					}
				}
			});

		// if none of the above states are matched, use this as the fallback
		$urlRouterProvider.otherwise('/tab/dash/');
		// $urlRouterProvider.otherwise('/login/');

	});
