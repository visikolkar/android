angular.module('eazybottle')

    .constant('SERVER', {
        // URL: 'http://www.eazybottle.jvmhost.net'
        URL: 'https://eazybottle.com'
    })

    .constant('PRODUCT', {
        ADD_A_BOTTLE: 1
    })

    .constant('$ionicLoadingConfig', {
        template: '<ion-spinner icon="android"></ion-spinner>',
        animation: 'fade-in',
        showBackdrop: true,
        showDelay: 0
    })

    .constant('PAYTM', {
        //PRODUCTION PAYTM VARIABLES
        INDUSTRY_TYPE_ID: "Retail109",
        CHANNEL_ID: "WEB",
        REQUEST_TYPE: "DEFAULT",
        MID: "EazyBo80812090291130",
        THEME: "merchant",
        CALLBACK_URL: "https://securegw.paytm.in/theia/paytmCallback?ORDER_ID=%s",
        WEBSITE: "EazyWAP"

        //STAGING PAYTM VARIABLES
        // INDUSTRY_TYPE_ID: "Retail",
        // CHANNEL_ID: "WAP",
        // REQUEST_TYPE: "DEFAULT",
        // MID: "EazyBo06509875098816",
        // THEME: "merchant",
        // CALLBACK_URL: "https://pguat.paytm.com/paytmchecksum/paytmCallback.jsp",
        // WEBSITE: "APP_STAGING"
    })