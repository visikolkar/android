angular.module('eazybottle')

	.controller('LoginCtrl', function ($scope, $state, $timeout, $ionicLoading, Login, Auth) {
		$scope.data = {
			show: true,
			//otpLoading: false
		};
		$scope.login = {
			phoneNumber: '',
			terms: true,
			otp: ''
		};
		$scope.counter = 30;
		$scope.loginUser = function () {
			console.log('login data is ', $scope.login);
			if (/^[0-9]{10}$/.test($scope.login.phoneNumber) && $scope.login.terms) {
				$ionicLoading.show({
					template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Requesting OTP</p>',
					animation: 'fade-in',
					showBackdrop: true,
					showDelay: 0
				});
				$scope.errMsg = '';//setting the error message back to empty
				var isAndroid = ionic.Platform.isAndroid();
				if (!SMS) { console.log('SMS plugin not ready'); }
				//else if (device.platform == "Android") {
				else if (isAndroid) {
					document.addEventListener('onSMSArrive', function (e) {
						var data = e.data.body;
						if (e.data.address.includes('EZYBOT')) {
							SMS.stopWatch(function () { }, function () { });

							//get otp from SMS
							var startidx = data.indexOf('is');
							if (startidx <= 0) return;

							var otp_data = data.substring(startidx + 4, startidx + 8);
							$scope.login.otp = otp_data;
							$scope.loginOTP();
						}
					});

					cordova.plugins.diagnostic.requestRuntimePermissions(function (statuses) {
						for (var permission in statuses) {
							switch (statuses[permission]) {
								case cordova.plugins.diagnostic.permissionStatus.GRANTED:
									SMS.startWatch(function () { }, function () { console.log('Failed to watch SMS'); });
									break;
							}
						}
					}, function (error) { console.error("SMS permission request. error occurred: " + error); },
						[cordova.plugins.diagnostic.permission.RECEIVE_SMS]);
				}
				Login.sendOTP({ phone: $scope.login.phoneNumber }).then(otpSuccess, otpFailure);
				//$scope.data.otpLoading = true;
				function otpSuccess(res) {
					console.log('otpSuccess response is ', res);
					$ionicLoading.hide();
					if (res.data.success) {
						window.plugins.toast.showShortBottom(
							"OTP sent successfuly", function (a) { }, function (b) { }
						);
						$scope.data = { show: false };
						//$scope.data.otpLoading = false;
						$scope.onTimeout = function () {
							$scope.counter--;
							if ($scope.counter > 0) {
								mytimeout = $timeout($scope.onTimeout, 1000);
							} else {
								$timeout.cancel(mytimeout);
							}
						};
						var mytimeout = $timeout($scope.onTimeout, 1000);
					} else {
						//show the toast with 
						window.plugins.toast.showShortBottom(
							res.data.errorMsg, function (a) { }, function (b) { }
						);
					}
				};
				function otpFailure(res) {
					console.log('otpFailure response is ', res);
				}
			} else if (!$scope.login.terms) {
				$scope.errMsg = 'Please agree to the terms and conditions to proceed';
			} else {
				$scope.errMsg = 'Please enter a valid 10 - digit mobile number';
			}
		};
		$scope.resendOTP = function () {
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Requesting OTP</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			Login.reSendOTP({ phone: $scope.login.phoneNumber }).then(otpSuccess, otpFailure);
			function otpSuccess(res) {
				$ionicLoading.hide();
				console.log('otpSuccess response is ', res);
				if (res.data.success) {
					window.plugins.toast.showShortBottom(
						"OTP re-sent successfuly", function (a) { }, function (b) { }
					);
				} else {
					//show the toast with 
					window.plugins.toast.showShortBottom(
						res.data.errorMsg, function (a) { }, function (b) { }
					);
					console.log('otpSuccess response false is ', res)
				}
			};
			function otpFailure(res) {
				console.log('otpFailure response is ', res);
			};
		};
		$scope.loginOTP = function () {
			console.log('login data with OTP is ', $scope.login);
			if ($scope.login.otp) {
				$ionicLoading.show({
					template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loading</p>',
					animation: 'fade-in',
					showBackdrop: true,
					showDelay: 0
				});
				Login.validateOTP({ phone: $scope.login.phoneNumber, otp: $scope.login.otp }).then(validateSuccess, validateFailure);
				function validateSuccess(res) {
					console.log('validateSuccess response is ', res);
					$ionicLoading.hide();
					if (res.data.success) {
						Auth.token(res.data.response)
						$state.go('tab.dash');
					} else {
						window.plugins.toast.showShortBottom(
							res.data.errorMsg, function (a) { }, function (b) { }
						);
					}
				};
				function validateFailure(res) {
					console.log('validateFailure response is ', res);
				}
			} else {
				$scope.errMsg = 'Please enter OTP to proceed';
			}
		};

	})

	// .controller('localnotificationhandler', function ($rootScope, $scope, $ionicPopup, $ionicPlatform, Order, Slots) {

	// 	$scope.notificationSlots = Slots.getTimelySlots();
	// 	$scope.itemSelction = function (id, arr) { //boiler plate code occurs in DashCtrl too
	// 		arr.map(function (obj) {
	// 			if (obj.id === id) {
	// 				obj.selected = true;
	// 			} else {
	// 				obj.selected = false;
	// 			}
	// 		});
	// 		console.log('updated arr is ', arr);
	// 		var selectedItem = arr.find(function (obj) {
	// 			if (obj.selected === true) {
	// 				return obj;
	// 			}
	// 		});
	// 		// console.log('selected item is ', selectedItem);
	// 		return selectedItem;
	// 	};
	// 	//only for local notification popup case
	// 	$scope.select_slot_localNotification = function (id) {
	// 		$scope.selectedSlot = $scope.itemSelction(id, $scope.notificationSlots);
	// 		console.log('selected slot is ', $scope.selectedSlot);
	// 		//add this to the order object
	// 	};//ends here

	// 	$scope.notiCallback = function (notification) {
	// 		console.log('User clicked  on notification');
	// 		notification.data = JSON.parse(notification.data);
	// 		$scope.showPopUp = function () {
	// 			//show action dialog
	// 			var deregisterBackButton = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
	// 			$scope.notipopup = $ionicPopup.show({
	// 				title: 'Upcoming delivery ' + getDeliveryTimeStr(notification.data.deliveryTime) + '. Please confirm',
	// 				scope: $scope,
	// 				buttons: [
	// 					{
	// 						type: 'button-popup-icon-confirm',
	// 						text: '<b class="icon ion-checkmark-circled"><br>Confirm</b>',
	// 						onTap: function (e) {
	// 							//confirmPopup.close();
	// 						}
	// 					},
	// 					{
	// 						type: 'button-popup-icon-reschedule',
	// 						text: '<b class="icon ion-clock"><br>Postpone</b>',
	// 						onTap: function (e) {
	// 							$scope.notipopup.close();
	// 							return $scope.postponepopup();
	// 						}
	// 					},
	// 					{
	// 						type: 'button-popup-icon-cancel',
	// 						text: '<b class="icon ion-close-circled"><br>Cancel</b>',
	// 						onTap: function (e) {
	// 							Order.cancelDelivery({ id: notification.data.deliveryId })
	// 								.then(function (result) {
	// 									if (result.success) {
	// 										//$scope.deliveryList = Order.fetchDeliveryList();
	// 										//var canceledDel = $scope.deliveryList.findIndex(function (value) {
	// 										//	return value.id === result.response.id;
	// 										//});
	// 										//$scope.deliveryList[canceledDel].status = result.response.status;
	// 										//Order.storeDeliveryList($scope.deliveryList);
	// 										$scope.user = $rootScope.prepareUserData(result.response.customer);
	// 										window.plugins.toast.showShortBottom(
	// 											"Cancelled", function (a) { }, function (b) { });
	// 									} else {
	// 										window.plugins.toast.showShortBottom(
	// 											"Failed to cancel", function (a) { }, function (b) { });
	// 									}
	// 								}, function (error) {
	// 									window.plugins.toast.showShortBottom(
	// 										"Failed to cancel", function (a) { }, function (b) { });
	// 								});
	// 						}
	// 					}
	// 				]
	// 			});
	// 			$scope.notipopup.then(function (res) {
	// 				deregisterBackButton();
	// 			});
	// 		};
	// 		$scope.showPopUp();
	// 		$scope.postponepopup = function () {
	// 			var deregisterBackButton2 = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
	// 			var date = new Date();
	// 			var hourDelivery = date.getHours();
	// 			var minutesDelivery = date.getMinutes();
	// 			if (hourDelivery >= 19 || (hourDelivery == 18 && minutesDelivery >= 30)) {
	// 				$scope.moroMsg = 'Slots for tomorrow';
	// 			} else {
	// 				$scope.moroMsg = 'Choose slot';
	// 			}
	// 			var popup = $ionicPopup.show({
	// 				title: $scope.moroMsg,
	// 				templateUrl: 'templates/popup-slots.html',
	// 				cssClass: 'postponepopup',
	// 				buttons: [
	// 					{
	// 						type: 'button-later',
	// 						text: 'Back',
	// 						onTap: function (e) {
	// 							popup.close();
	// 							$scope.showPopUp();
	// 						}
	// 					},
	// 					{
	// 						type: 'button-get-now',
	// 						text: 'Confirm',
	// 						onTap: function (e) {
	// 							Order.postponeDelivery({ id: notification.data.deliveryId, slot_id: $scope.selectedSlot.id })
	// 								.then(function (result) {
	// 									if (result.success) {
	// 										window.plugins.toast.showShortBottom(
	// 											"Postponed", function (a) { }, function (b) { });
	// 									} else {
	// 										window.plugins.toast.showShortBottom(
	// 											"Failed to postpone", function (a) { }, function (b) { });
	// 									}
	// 								}, function (error) {
	// 									window.plugins.toast.showShortBottom(
	// 										"Failed to postpone", function (a) { }, function (b) { });
	// 								});
	// 						}
	// 					}]
	// 			});
	// 			popup.then(function (res) {
	// 				deregisterBackButton2();
	// 			});
	// 		};
	// 	};
	// })
	.controller('DashCtrl', function ($rootScope, $scope, $state, $ionicModal, $ionicPopup, $ionicLoading, $cordovaGeolocation, $ImageCacheFactory, $ionicPlatform, $ionicDeploy, Slots, Vendors, Products, Address, Payment, Order, UserService, PRODUCT, SERVER, PAYTM) {

		$scope.user = UserService.fetchUserInfo();
		console.log('user outer scope variable', $scope.user)
		$scope.slots = Slots.fetchSlots();
		//$scope.vendors = Vendors.fetchVendors();  moved to $scope.vendors = $scope.vendorFromUserAddress();
		//$scope.product = Products.fetchProducts();
		$scope.businessArea = Address.fetchBusinessArea();
		$scope.businessValues = Address.fetchBusinessValues();
		//$scope.notificationSlots = Slots.getTimelySlots();

		$scope.monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];
		$scope.isAuto = {
			selection: ''
		};
		$scope.schedule = {
			date: '',
			upcoming: ''
		};
		$scope.upcomingMessage = "Use Auto Delivery"; //initial value

		if($scope.businessValues){
			//home screen carousel autoplay settings
			$scope.sliderShow = true;
			$scope.options = {
				loop: true,
				speed: 1000,
				autoplay: 3000,
				autoplayDisableOnInteraction: false
			};
		} else{
			$scope.sliderShow = false;
		}

		$scope.checkForUpdate = function () {
			//ionic deploy starts here
			$ionicDeploy.channel = 'production';
			$ionicDeploy.check().then(function (snapshotAvailable) {
				if (snapshotAvailable) {
					// When snapshotAvailable is true, you can apply the snapshot
					console.log('new snapshot is available');
					var deregisterBackButton = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
					var updatePopUp = $ionicPopup.show({
						// templateUrl: 'app/popup/userTimePopUp.html',
						title: 'Please update App to continue to use',
						scope: $scope,
						buttons: [
							{
								text: '<b>Update</b>',
								type: 'button-get-now',
								onTap: function (e) {
									console.log('Ok! lets update the app');
									var update = window.open('https://play.google.com/store/apps/details?id=com.eazybottle.android', '_system', 'location=no');
									return update;
								}
							}
						]
					});
					updatePopUp.then(function (res) {
						deregisterBackButton();
					});
				} else {
					console.log('no new snapshotAvailable', snapshotAvailable);
				}
			}, function (err) {
				console.error('unable to check for an update', err);
			});
		};

		//watch user variable
		$scope.$watch('$scope.user', function () {
			console.log('hey, user has changed!', $scope.user);
		});
		$scope.$on('$ionicView.enter', function (e) {
			console.log('checkin the controller initialization');
			$scope.refreshUserInfo();
			//$scope.showConfirm();//only for testing
			$scope.checkForUpdate();
		});

		$scope.updateUpcomingDate = function (mili) {
			var d = new Date(mili);
			console.log('value of d is ', d);
			$scope.schedule.upcoming = $scope.monthNames[d.getMonth()] + ' ' + d.getDate() + ', ' + d.getFullYear();
			$scope.upcomingMessage = 'Upcoming Delivery' + $scope.schedule.upcoming;
		};

		$scope.refreshUserInfo = function () {
			//user api call
			UserService.getInfo()
				.then(function (result) {
					if (result.data.success) {
						console.log('User promise is resolved', result);
						$scope.user = $rootScope.prepareUserData(result.data.response);
						if ($scope.user.scheduledTime && +$scope.user.walletBottles && $scope.user.isAutoDelivery && $scope.user.depositAmt) {
							$scope.updateUpcomingDate($scope.user.scheduledTime);
						} else {
							$scope.upcomingMessage = "Use Auto Delivery";
						}
						if ($scope.user.newUser) { //&& +$scope.user.walletBottles
							$scope.showConfirm(); //call this function only when it's the New User.
						} else {
							//no popup message
						}
						console.log('user inner scope variable', $scope.user)
						UserService.storeInfo($scope.user); //store user data into local storage for faster retrival
						if($scope.user.currentAddress){
							var dateDelivery = new Date();
							var hourDelivery = dateDelivery.getHours();
							var minutesDelivery = dateDelivery.getMinutes();
							var nextHoliday = new Date($scope.user.currentAddress.businessArea.nextHoliday);
							if(nextHoliday.getDate() >= dateDelivery.getDate() && nextHoliday.getMonth() == dateDelivery.getMonth() && nextHoliday.getFullYear() == dateDelivery.getFullYear()){
								$scope.holidayShow = true;
								$scope.holidayMsg = "Holiday on "+$scope.monthNames[nextHoliday.getMonth()] +", "+ nextHoliday.getDate();
							} else{
								$scope.holidayShow = false;
								$scope.holidayMsg = "";
							}
						} else{
							$scope.holidayShow = false;
							$scope.holidayMsg = "";
						}
					} else {
						console.log('User promise something went wrong');
					}
				}, function (error) {
					console.log('User promise is rejected', error);
				});
		};

		//slots api call
		Slots.getSlotsList()
			.then(function (result) {
				console.log('slots promise is resolved', result);
				if (result.success) {
					$scope.slots = result.response;
					Slots.storeSlots(result.response);
				} else {
					console.log('slots promise something went wrong');
				}
			}, function (error) {
				console.log('slots promise is rejected', error);
			});

		//vendors list api
		// Vendors.getVendors()
		// 	.then(function (result) {
		// 		console.log('vendors promise is resolved', result);
		// 		if (result.success) {
		// 			result.response.map(function (obj) {
		// 				obj.url = SERVER.URL + obj.url
		// 				obj.reports.map(function (o) {
		// 					o.url = SERVER.URL + o.url;
		// 					return o;
		// 				});
		// 				return obj;
		// 			});
		// 			$scope.vendors = result.response;
		// 			Vendors.storeVendors(result.response);
		// 			//Build the list of images to preload
		// 			var images = [];
		// 			$scope.vendors.forEach(function (elm) {
		// 				images.push(elm.url);
		// 				elm.reports.forEach(function (rep) {
		// 					images.push(rep.url);
		// 				});
		// 			});
		// 			console.log('cached images are ', images);
		// 			$ImageCacheFactory.Cache(images);
		// 		} else {
		// 			console.log('vendors promise something went wrong');
		// 		}
		// 	}, function (error) {
		// 		console.log('vendors promise is rejected', error);
		// 	});

		//products list api
		// Products.getProducts()
		// 	.then(function (result) {
		// 		console.log('products promise is resolved', result);
		// 		if (result.success) {
		// 			$scope.product = result.response;
		// 			Products.storeProducts(result.response);
		// 		} else {
		// 			console.log('vendors promise something went wrong');
		// 		}
		// 	}, function (error) {
		// 		console.log('products promise is rejected', error);
		// 	});

		//businessArea list api call
		Address.businessAreaList()
			.then(function (result) {
				console.log('businessAreaList promise is resolved', result);
				if (result.success) {
					$scope.businessArea = result.response;
					Address.storeBusinessArea(result.response);
					if ($scope.businessArea) { // this gotta move outside of the promise 
						$scope.polygons = [];
						$scope.businessArea.forEach(function (obj) {
							console.log("obj.polygon is ", obj.polygon);
							$scope.polygons.push(JSON.parse(obj.polygon));
							// $scope.polygons.push(obj.polygon);
						});
						console.log('businessArea polygon is ', $scope.polygons);
					}
				} else {
					console.log('businessAreaList promise something went wrong');
				}
			}, function (error) {
				console.log('businessAreaList promise is rejected', error);
			});
		//business values
		Address.businessValues()
		.then(function (result) {
			console.log('businessValue promise is resolved', result);
			if (result.success) {
				$scope.businessValues = result.response;
				Address.storeBusinessValues(result.response);
				$ImageCacheFactory.Cache($scope.businessValues.carouselImages);
				$scope.sliderShow = true;
				$scope.options = {
					loop: true,
					speed: 1000,
					autoplay: 3000,
					autoplayDisableOnInteraction: false
				};
			} else {
				console.log('businessValues promise something went wrong');
			}
		}, function (error) {
			console.log('businessValues promise is rejected', error);
		});
		
		//exchange bottle Object initialization
		$scope.exchange = {
			bottle: false
		};

		//order object for Wallet Purchase
		$scope.order = {
			total: '',
		};
		// getDeliveryNow object for deliveries
		$scope.getDeliveryNow = {
			total: '',
			deposit: '',
			bottles: 1,
			is_exchange: '',
			delivery_type: '',
			numberOfBottles: 1
		};

		// set product color
		$scope.set_color = function (id) {
			if (id == 1) {
				return { "background-color": "#BBDEFB" };
			} else if (id == 'Silver Offer') {
				return { "background-color": "#6fdc6f" }; //#C0C0C0
			} else if (id == 'Gold Offer') {
				return { "background-color": "#D5A458" }; //FDD835
			} else if (id == 'Platinum Offer') {
				return { "background-color": "#79CEDC" }; //E0E0E0
			} else{
				return { "background-color": "#BA68C8" }; //special offer
			}
		};

		// A confirm dialog for First Bottle "Get Delivery Now"
		$scope.showConfirm = function () {
			console.log('popup function is called');
			var deregisterBackButton = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
			var confirmPopup = $ionicPopup.show({
				title: 'Click <b>Water</b> to Order your first FREE Bottle',
				scope: $scope,
				buttons: [
					{
						text: 'Later',
						type: 'button-later',
						onTap: function (e) {
							confirmPopup.close();
						}
					},
					{
						text: '<b>Get Delivery Now</b>',
						type: 'button-get-now',
						onTap: function (e) {
							$scope.fetchBottleAddress();
						}
					}
				]
			});

			confirmPopup.then(function (res) {
				deregisterBackButton();
			});
		};

		//wallet modal page
		$ionicModal.fromTemplateUrl('templates/modal-wallet.html', {
			id: '1',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal1 = modal;
		});
		//Instant Bottle modal page
		$ionicModal.fromTemplateUrl('templates/modal-instant.html', {
			id: '2',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal2 = modal;
		});
		//Auto Delivery modal
		$ionicModal.fromTemplateUrl('templates/modal-auto.html', {
			id: '3',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal3 = modal;
		});
		//Addess Modal
		$ionicModal.fromTemplateUrl('templates/modal-address.html', {
			id: '4',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal4 = modal;
		});
		$scope.openModal = function (index) {
			if (index == 1) $scope.oModal1.show();
			else if (index == 2) $scope.oModal2.show();
			else if (index == 3) $scope.oModal3.show();
			else if (index == 4) $scope.oModal4.show();
		};
		$scope.closeModal = function (index) {
			if (index == 1) $scope.oModal1.hide();
			else if (index == 2) $scope.oModal2.hide();
			else if (index == 3) $scope.oModal3.hide();
			else if (index == 4) $scope.oModal4.hide();
		};

		$scope.vendorFromUserAddress = function () {
			var result = [];
			if ($scope.user.currentAddress.businessArea) {
				var vendorArray = $scope.user.currentAddress.businessArea.vendors;
				vendorArray.forEach(function (obj) {
					result.push(obj);
				});
				// return result;
				result.map(function (obj) {
					if (!obj.url.startsWith('http')) {
						obj.url = SERVER.URL + obj.url;
						obj.reports.map(function (o) {
							o.url = SERVER.URL + o.url;
							return o;
						});
						return obj;
					} else {
						return obj;
					}
				});
				Vendors.storeVendors(result);
				//Build the list of images to preload
				var images = [];
				result.forEach(function (elm) {
					images.push(elm.url + '.png');
					elm.reports.forEach(function (rep) {
						images.push(rep.url + '.png');
					});
				});
				console.log('cached images are ', images);
				$ImageCacheFactory.Cache(images);
				console.log('vendor from user address is ', result);
				$scope.availableVendor = true;
				return result;
			} else {
				//no vendor available for current address
				$scope.availableVendor = false;
				return 0;
			}
		};

		//fetchBottleAddress and display vendors accordingly
		$scope.fetchBottleAddress = function () {
			if (!!$scope.user.currentAddress) {
				//$scope.vendors = $scope.vendorFromUserAddress(); 
				console.log('fectchBottleAddress: user address exists open the instant bottle page');
				$scope.openModal(2);
			} else {
				console.log('fectchBottleAddress: user address does not exists open the address page');
				$scope.openModal(4);
			}
		};

		$scope.checkMaintenance = function(){
			console.log('business valaues are ', $scope.businessValues);
			if($scope.businessValues){
				//business values are loaded from api
			}else{
				//if api loaded late or any other issues fetch the business values from localStorage
				$scope.businessValues = Address.fetchBusinessValues();
			}
			if($scope.user.currentAddress.businessArea){
				var holiday = new Date();
				var holidayDate = holiday.getDate();
				var holidayMonth = holiday.getMonth();
				var nextHoliday = new Date($scope.user.currentAddress.businessArea.nextHoliday);
				if ($scope.businessValues.maintenance || (nextHoliday.getDate() == holidayDate && nextHoliday.getMonth() == holidayMonth && nextHoliday.getFullYear() == holiday.getFullYear())) { //|| $scope.user.currentAddress.businessArea.holiday
					var deregisterBackButton = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
					var maintenancePopUp = $ionicPopup.show({
						title: 'Thank you for your support! Today EazyBottle 20L service is under maintenance.',
						scope: $scope,
						buttons: [
							{
								text: '<b>Thank you!</b>',
								type: 'button-get-now',
								onTap: function (e) {
									$scope.closeModal(2);
								}
							}
						]
					});
					maintenancePopUp.then(function (res) {
						deregisterBackButton();
					});
				}
			}
		};

		$scope.serviceNotAvailable = function(){
			var deregisterBackButton = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
			var name = '';
			if($scope.user.name){
				name = $scope.user.name;
			} else{
				name = "EazyBottler"
			}
			var servicePopUp = $ionicPopup.show({
				title: '<b>Dear '+ name +'</b> EazyBottle service is not available at your current address. Click Notify Me to stay updated on our expansion news. Thank you so much!',
				scope: $scope,
				buttons: [
					{
						text: 'Edit Address',
						type: 'button-later',
						onTap: function (e) {
							$scope.updateAddress();
						}
					},
					{
						text: '<b>Notify Me</b>',
						type: 'button-get-now',
						onTap: function (e) {
							$scope.closeModal(2);
							var update = window.open('https://www.facebook.com/eazybottle/', '_system', 'location=no');
							return update;
						}
					}
				]
			});
			servicePopUp.then(function (res) {
				deregisterBackButton();
			});
		};

		$scope.$on('modal.shown', function (event, modal) {
			console.log('Modal ' + modal.id + ' is shown!');
			if (modal.id === '1') { //modal-wallet.html
				var walletVendor = [];
				$scope.user.bottles.forEach(function (obj) {
					if (obj.refundStatus == 'NONE' && obj.depositAmt) { //check if the bottles are requested for refund
						walletVendor.push(obj.vendor);
					}
				});
				$scope.vendors = walletVendor;
				console.log('vendor info for wallet is', $scope.vendors);
				$scope.select_vendor($scope.vendors[0].id);
			} else if (modal.id === '2') { //modal-instant.html
				$scope.checkMaintenance();
				// if ($scope.user.newUser && !+$scope.user.walletBottles) { //after the first bottle delivery request, blocking the user from modal-instant page
				// 	var deregisterBackButton = $ionicPlatform.registerBackButtonAction(function (e) { }, 401);
				// 	var firstBottlePopUp = $ionicPopup.show({
				// 		title: 'Your bottle is under transit. You may TRACK from the ORDER section!',
				// 		scope: $scope,
				// 		buttons: [
				// 			{
				// 				text: '<b>Okay</b>',
				// 				type: 'button-get-now',
				// 				onTap: function (e) {
				// 					$scope.closeModal(2);
				// 				}
				// 			}
				// 		]
				// 	});
				// 	firstBottlePopUp.then(function (res) {
				// 		deregisterBackButton();
				// 	});
				// }//need to test this condition
				$scope.getDeliveryNow.bottles = false;
				$scope.getDeliveryNow.numberOfBottles = 1; //initially
				$scope.item = {quantity : 1};
				$scope.quickDeliveryCharge = 0; //initial value
				if ($scope.user.currentAddress.isServiceable) {
					$scope.availableVendor = true;
				} else {
					$scope.availableVendor = false;
				}
				//showing the vendors for customer wallet bottles [this is also applicable for multiple vendor bottles use case]
				if($scope.user.currentAddress.businessArea){
					if(+$scope.user.walletBottles){
						var vendorSearch = $scope.user.currentAddress.businessArea.vendors;
						var resultVendor = $scope.user.bottles.filter(function(o1){
							// filter out matching items
							return vendorSearch.some(function(o2){
								return o1.vendor.id === o2.id;
							});
						});
						console.log('resultVendor is ', resultVendor);
					}
					if (+$scope.user.walletBottles && resultVendor.length) {
						$scope.wallet = true; //wallet variable for refund bottles scenario
						var walletVendor = [];
						resultVendor.forEach(function (obj) {
							if (obj.free+obj.paid) { // obj.depositAmt && 
								walletVendor.push(obj.vendor);
							} else{
								
							}
						});
						$scope.vendors = $scope.vendorFromUserAddress();//walletVendor; //show all the vendors irrespective of wallet bottles
					} else {
						$scope.wallet = false;
						$scope.vendors = $scope.vendorFromUserAddress();
					}
				} else{
					$scope.availableVendor = false;
					$scope.serviceNotAvailable();
					return 0; 
				}
				
				$scope.moroShow = false;
				$scope.select_vendor($scope.vendors[0].id);
				$scope.deliveryShow = false;
				$scope.deliverySlots = false;
				//need to test the time conditions
				//$scope.delivery = [{ name: 'Normal Delivery', id: '1', time: 'Within 2 hours', type: 'NORMAL' }, { name: 'Quick Delivery', id: '2', time: 'Within 30 mins', type: 'QUICK' }, { name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				var dateDelivery = new Date();
				var hourDelivery = dateDelivery.getHours();
				var minutesDelivery = dateDelivery.getMinutes();
				//freshly
				if (hourDelivery >= 8) {
					$scope.delivery = [{ name: 'Normal Delivery', id: '1', time: 'Within 2 hours', type: 'NORMAL' }, { name: 'Quick Delivery', id: '2', time: 'Within 30 mins', type: 'QUICK' }, { name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				}
				if ((hourDelivery == 11 && minutesDelivery > 30) || hourDelivery == 12) {
					$scope.delivery = [{ name: 'Quick Delivery', id: '2', time: 'Within 30 mins', type: 'QUICK' }, { name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				}
				if ((hourDelivery == 12 && minutesDelivery > 30) || hourDelivery == 13 || hourDelivery == 14) {
					$scope.delivery = [{ name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				}
				// if(hourDelivery == 15 || hourDelivery == 16 || hourDelivery == 17 || hourDelivery == 18 ){
				// 	$scope.delivery = [{ name: 'Normal Delivery', id: '1', time: 'Within 2 hours', type: 'NORMAL' }, { name: 'Quick Delivery', id: '2', time: 'Within 30 mins', type: 'QUICK' }, { name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				// }
				if (hourDelivery == 19 || hourDelivery == 20) {
					$scope.delivery = [{ name: 'Quick Delivery', id: '2', time: 'Within 30 mins', type: 'QUICK' }, { name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				}
				if ((hourDelivery >= 20 && minutesDelivery >= 30) || hourDelivery < 8 || hourDelivery >= 21) {
					$scope.delivery = [{ name: 'Schedule Delivery', id: '3', time: '', type: 'SCHEDULE' }];
				}
				$scope.slots = Slots.getTimelySlots();
				console.log('available slots are ', $scope.slots);
				// $scope.vendors = [{id: 1, name: 'Pearl Drops', image: 'img/nandu.png', price: 40}, {id: 2, name: 'Bisleri', image: 'img/nandu.png', price: 80}];
			} else if (modal.id === '3') { //modal-auto.html
				// show a toast if the wallet is NULL "Auto Delivery is best when you've Bottles in Wallet"
				$scope.user = UserService.fetchUserInfo();
				$scope.slots = Slots.fetchSlots();
				$scope.days = [{ id: 1, time: 'Every Day' }, { id: 2, time: 'Every 2 Days' }, { id: 3, time: 'Every 3 Days' }, { id: 4, time: 'Every 4 Days' }, { id: 5, time: 'Every 5 Days' }, { id: 6, time: 'Every 6 Days' }, { id: 7, time: 'Every 7 Days' }, { id: 8, time: 'Every 8 Days' }, { id: 9, time: 'Every 9 Days' }, { id: 10, time: 'Every 10 Days' }, { id: 11, time: 'Every 11 Days' }, { id: 12, time: 'Every 12 Days' }, { id: 13, time: 'Every 13 Days' }, { id: 14, time: 'Every 14 Days' }, { id: 15, time: 'Every 15 Days' }];

				//Auto days settings
				if ($scope.user.deliveryFrequency) {
					$scope.selectedDay = $scope.itemSelction($scope.user.deliveryFrequency, $scope.days);
				} else {
					//user haven't set the auto days yet
				}

				//Auto slot settings
				if ($scope.user.slot) {
					$scope.selectedAutoSlot = $scope.itemSelction($scope.user.slot.id, $scope.slots);
				} else {
					//no slots yet
				}

				//Auto delivery button settings
				if ($scope.user.isAutoDelivery && +$scope.user.walletBottles) {
					$scope.isAuto.selection = true;
					$scope.updateUpcomingDate($scope.user.scheduledTime);
				} else {
					//no upcoming delivery set
				}

			} else if (modal.id === '4') { //modal-address.html
				$scope.address.phone = $scope.user.phone; //set user phone number after the modal is open
				$scope.getMap();
				if ($scope.user.currentAddress) {
					$scope.address.name = $scope.user.currentAddress.name;
					$scope.address.streetAddress = $scope.user.currentAddress.streetAddress;
				}
			}
		});


		// Execute action on hide modal
		$scope.$on('modal.hidden', function (event, modal) {
			console.log('Modal ' + modal.id + ' is hidden!');
			if (modal.id === '2') {
				//setting the delivery options back to empty
				$scope.selectedDelivery = '';
				$scope.selectedSlot = '';
			}

		});
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function () {
			console.log('Destroying modals...');
			$scope.oModal1.remove();
			$scope.oModal2.remove();
			$scope.oModal3.remove();
			$scope.oModal4.remove();
		}); //modal end here

		//Add bottles to Wallet
		$scope.addBottles = function (id) { //will not have an id as an argument
			console.log('go to wallet purchse modal');
			if ($scope.user.newUser) {
				window.plugins.toast.showLongTop(
					"TRY before you BUY! Click Get Delivery Now for FREE Bottle!", function (a) { }, function (b) { }
				);
				return false;
			}
			if ($scope.user.isRefund) {
				window.plugins.toast.showLongTop(
					"Deposit Refund is in Process", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.user.depositAmt) {
				window.plugins.toast.showLongTop(
					"Click Get Delivery Now!", function (a) { }, function (b) { }
				);
				return false;
			}
			//$scope.select_item('Silver Offer');
			$scope.openModal(1);
			// $scope.product[id].selected = true;

		};
	
		$scope.sub = function () {
			if($scope.item.quantity == 1){
				//don't decrease
			} else{
				$scope.item.quantity--;
				$scope.getDeliveryNow.numberOfBottles = 1;
			}
		};

		$scope.add = function () {
			if($scope.item.quantity == 1){
				$scope.item.quantity++;
				$scope.getDeliveryNow.numberOfBottles = 2;
			} else{
				//don't increase
				window.plugins.toast.showLongTop(
					"Maximum of two bottles!", function (a) { }, function (b) { }
				);
			}
		};

		$scope.exchangeBottle = function () {
			console.log('click event ', $scope.exchange.bottle);
			if ($scope.exchange.bottle) {
				$scope.getDeliveryNow.deposit = $scope.selectedVendor.exchangeDepositAmt;//70;
				// $scope.getDeliveryNow.total -= 50;
				$scope.getDeliveryNow.is_exchange = true;
			} else {
				$scope.getDeliveryNow.deposit = $scope.selectedVendor.depositAmt;//120;
				// $scope.getDeliveryNow.total += 50;
				$scope.getDeliveryNow.is_exchange = false;
			}
		};

		$scope.deliveryBottleNumber = function () {
			console.log('delivery bottle bumber has changed', $scope.getDeliveryNow.bottles);
			if ($scope.getDeliveryNow.bottles) {
				$scope.getDeliveryNow.numberOfBottles = 2;
			} else {
				$scope.getDeliveryNow.numberOfBottles = 1;
			}
		}

		$scope.depositMsg = function () {
			window.plugins.toast.showLongBottom(
				"Upgrade to New Bottle. Get Deposit Money back upon Request!", function (a) { }, function (b) { }
			);
		};

		$scope.itemSelction = function (id, arr) {
			arr.map(function (obj) {
				if (obj.id === id) {
					obj.selected = true;
				} else {
					obj.selected = false;
				}
			});
			console.log('updated arr is ', arr);
			var selectedItem = arr.find(function (obj) {
				if (obj.selected === true) {
					return obj;
				}
			});
			// console.log('selected item is ', selectedItem);
			return selectedItem;
		};
		// product selection
		$scope.select_item = function (id) {
			$scope.selectedProduct = $scope.itemSelction(id, $scope.product); //product array is passed for itemSelction
			console.log('selected product is ', $scope.selectedProduct);
			// $scope.order.total = $scope.selectedProduct.money; //don't add the deposit to the total here, add it when the modal is shown
		};

		//Vendor selection
		$scope.select_vendor = function (id) {
			//$scope.vendors = $scope.vendorFromUserAddress();
			$scope.selectedVendor = $scope.itemSelction(id, $scope.vendors); //vendors array is passed for itemSelction
			console.log('selected vendor is ', $scope.selectedVendor);
			$scope.exchange.bottle = false; // or call $scope.exchangeBottle()
			$scope.getDeliveryNow.is_exchange = false; //or call $scope.exchangeBottle()
			$scope.getDeliveryNow.bottles = false;
			$scope.getDeliveryNow.numberOfBottles = 1; //initially
			$scope.item = {quantity : 1};
			//after the vendor is selected 
			//fetch the product list from the selected vendor
			//from the product offers will be displayed
			//$scope.productFromVendor = $scope.selectedVendor.products;
			$scope.product = $scope.selectedVendor.products[0].offers;
			console.log('selected vendor product is ', $scope.selectedVendor);
			$scope.product.map(function (obj) {
				//obj.vendors = $scope.vendors;
				obj.money = (obj.noOfBottles * $scope.selectedVendor.products[0].price) - (obj.noOfBottles * obj.discount);
				obj.save = obj.noOfBottles * obj.discount;
				if (obj.save == 0) {
					obj.save = '';
				} else {
					obj.save = '₹ ' + obj.save;
				}
				return obj;
			});
			console.log('offer/product object is ', $scope.product);
			$scope.select_item($scope.product[0].id);

			//check for the selected vendor against the user deposit paid vendor
			$scope.isVendorPresent = $scope.user.bottles.find(function(obj){
				return (obj.vendor.id === $scope.selectedVendor.id && obj.depositAmt > 0);
				// if(obj.vendor.id === $scope.selectedVendor.id && obj.depositAmt){
				// 	return obj;
				// }
			});
			console.log('isVendorPresent is ', $scope.isVendorPresent);
			if($scope.isVendorPresent){
				//$scope.getDeliveryNow.deposit = 0;
				if(!$scope.isVendorPresent.depositAmt){
					$scope.getDeliveryNow.deposit = $scope.selectedVendor.depositAmt;
					$scope.showPaymentOptions = true;
					$scope.showConfirmDelivery = false;
				} else{
					$scope.getDeliveryNow.deposit = 0; //set back deposit amount to zero 
				}
				$scope.vendorBottleCount = $scope.isVendorPresent.free + $scope.isVendorPresent.paid;
				if($scope.isVendorPresent.refundStatus == 'NONE' && !$scope.vendorBottleCount && $scope.isVendorPresent.depositAmt){
					$scope.showPaymentOptions = true;
					$scope.showConfirmDelivery = false;
				}
				if($scope.isVendorPresent.refundStatus == 'NONE' && $scope.vendorBottleCount && $scope.isVendorPresent.depositAmt){
					$scope.showConfirmDelivery = true;
					$scope.showPaymentOptions = true;
				}
				if($scope.isVendorPresent.refundStatus == 'INITIATED'){
					//we can ask for one more deposit or simply ask the user to cancel the refund request
					$scope.getDeliveryNow.deposit = $scope.selectedVendor.depositAmt;
					$scope.showPaymentOptions = false;
					$scope.showConfirmDelivery = false;
					$scope.vendorBottleCount = 0; //for refund request set bottle count to zero
				}
			} else{
				$scope.vendorBottleCount = 0;
				$scope.showPaymentOptions = true;
				$scope.showConfirmDelivery = false;
				$scope.getDeliveryNow.deposit = $scope.selectedVendor.depositAmt;
			}

			if ($scope.vendorBottleCount || ($scope.user.newUser && $scope.selectedVendor.signupOffer)) { //if wallet is true or newUser the discount is calculated
				$scope.selectedVendor.discount = $scope.selectedVendor.products[0].price;//$scope.selectedVendor.price;
			} else {
				$scope.selectedVendor.discount = 0;
			}

		};

		//delivery selection
		$scope.select_delivery = function (id) {
			$scope.selectedDelivery = $scope.itemSelction(id, $scope.delivery);
			console.log('selected delivery is ', $scope.selectedDelivery);
			if ($scope.selectedDelivery.id == 1) {
				$scope.deliveryShow = true;
				$scope.moroShow = false;
				$scope.deliverySlots = false;
				$scope.deliveryMsg = 'Bottle will arrive within 2 hours';
				$scope.selectedSlot = { id: '2Hours' };
				$scope.quickDeliveryCharge = 0;
			} 
			if ($scope.selectedDelivery.id == 2) {
				$scope.deliveryShow = true;
				$scope.moroShow = false;
				$scope.deliverySlots = false;
				$scope.deliveryMsg = 'Bottle will arrive within 30 mins. Charges applicable!';
				$scope.selectedSlot = { id: '30Mins' };
				$scope.quickDeliveryCharge = $scope.businessValues.quickDeliveryCharge;//20;
			}
			if($scope.selectedDelivery.id == 3) {
				$scope.deliveryShow = false;
				$scope.deliverySlots = true;
				$scope.quickDeliveryCharge = 0;
				$scope.select_slot($scope.slots[0].id);
				var date = new Date();
				var hourDelivery = date.getHours();
				var minutesDelivery = date.getMinutes();
				if (hourDelivery >= 19 || (hourDelivery == 18 && minutesDelivery >= 30)) {
					$scope.moroShow = true;
					$scope.moroMsg = 'Slots for tomorrow';
				} else {
					$scope.moroShow = false;
				}
			}
		};

		$scope.select_slot = function (id) {
			$scope.selectedSlot = $scope.itemSelction(id, $scope.slots);
			console.log('selected slot is ', $scope.selectedSlot);
			//add this to the order object
		};

		$scope.select_auto_slot = function (id) {
			$scope.selectedAutoSlot = $scope.itemSelction(id, $scope.slots);
			console.log('selected slot is ', $scope.selectedAutoSlot);
			Slots.setAutoSlot({ slot_id: $scope.selectedAutoSlot.id })
				.then(function (result) {
					console.log('selectedAutoSlot promise is resolved', result);
					if (result.success) {
						$scope.user = $rootScope.prepareUserData(result.response);
						//Auto delivery button settings
						if ($scope.user.isAutoDelivery && +$scope.user.walletBottles) {
							$scope.isAuto.selection = true;
							$scope.updateUpcomingDate($scope.user.scheduledTime);
						} else {
							//no upcoming delivery set
						}
						window.plugins.toast.showShortTop(
							"Slot updated!", function (a) { }, function (b) { }
						);
					} else {
						console.log('selectedAutoSlot promise something went wrong');
					}
				}, function (error) {
					console.log('selectedAutoSlot promise is rejected', error);
				});
		};

		$scope.select_day = function (id) {
			$scope.selectedDay = $scope.itemSelction(id, $scope.days);
			console.log('selected slot is ', $scope.selectedDay);
			//update server 
			Slots.setAutoDays({ days: $scope.selectedDay.id })
				.then(function (result) {
					console.log('setAutoDays promise is resolved', result);
					if (result.success) {
						$scope.user = $rootScope.prepareUserData(result.response);
						if ($scope.user.depositAmt && +$scope.user.walletBottles) {
							$scope.updateUpcomingDate($scope.user.scheduledTime);
						}
						//Auto delivery button settings
						if ($scope.user.isAutoDelivery && +$scope.user.walletBottles) {
							$scope.isAuto.selection = true;
							$scope.updateUpcomingDate($scope.user.scheduledTime);
						} else {
							//no upcoming delivery set
						}
						window.plugins.toast.showShortTop(
							"Auto Days updated!", function (a) { }, function (b) { }
						);
					} else {
						console.log('setAutoDays promise something went wrong');
					}
				}, function (error) {
					console.log('setAutoDays promise is rejected', error);
				});
		};

		$scope.address = {
			name: '',
			phone: '',
			locality: '',
			default: true //gotta check this one
		};

		$scope.saveAddress = function (form) {
			if (form.$valid) {
				if (Address.getUpdateFlag()) {
					$scope.address.id = $scope.user.currentAddress.id;
					$ionicLoading.show({
						template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Updating Address...</p>',
						animation: 'fade-in',
						showBackdrop: false,
						showDelay: 0
					});
					// var polygon = [[12.92179, 77.66083],[12.91699, 77.67384],[12.91763, 77.67617],[12.92167, 77.67822],[12.9254, 77.67978],[12.9276, 77.68047],[12.92904, 77.67996],[12.934, 77.67802],[12.93084, 77.66569]];
					// var polygons = [polygon, polygon];
					var isServiceable = Address.checkUserPoint([$scope.address.lat, $scope.address.lng], $scope.polygons);

					// var isServiceable = Address.checkUserPoint($scope.address.lat, $scope.address.lng);
					if (isServiceable) {
						$scope.address.isServiceable = true;
					} else {
						$scope.address.isServiceable = false;
					}
					Address.updateAddress($scope.address)
						.then(function (result) {
							$ionicLoading.hide();
							console.log('update address promise is resolved', result);
							if (result.success) {
								$scope.user.currentAddress = result.response[result.response.length - 1];
								UserService.storeInfo($scope.user); //store updated user address to localStorage
								$scope.user = UserService.fetchUserInfo();//this is not required
								$scope.getDeliveryNow.addressId = $scope.user.currentAddress.id;
								console.log('data after the address update api call', $scope.address);
								Address.setUpdateFlag(false);
								$scope.closeModal(4);
								if ($scope.address.isServiceable) {
									//dynamicAlert.showAlert('you are reachable! Enjoy the Service');
									$scope.openModal(2); //open instant modal after update address 
									window.plugins.toast.showShortBottom(
										"Address updated!", function (a) { }, function (b) { }
									);
								} else {
									//dynamicAlert.showAlert('Currently we are not available at your address. Stay Tuned');
									window.plugins.toast.showLongBottom(
										"Currently we are not reachable at your address. Stay Tuned!", function (a) { }, function (b) { }
									);
								}
							} else {
								console.log('something not good while updating address', result);
							}
						}, function (error) {
							console.log('update address promise is rejected', error);
							$ionicLoading.hide();
						});

				} else {
					console.log('user address is ', $scope.address);
					$ionicLoading.show({
						template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Saving Address...</p>',
						animation: 'fade-in',
						showBackdrop: false,
						showDelay: 0
					});
					// var polygon = [[12.92179, 77.66083],[12.91699, 77.67384],[12.91763, 77.67617],[12.92167, 77.67822],[12.9254, 77.67978],[12.9276, 77.68047],[12.92904, 77.67996],[12.934, 77.67802],[12.93084, 77.66569]];
					// var polygons = [polygon, polygon];
					var isServiceable = Address.checkUserPoint([$scope.address.lat, $scope.address.lng], $scope.polygons);
					if (isServiceable) {
						$scope.address.isServiceable = true;
					} else {
						$scope.address.isServiceable = false;
					}

					Address.saveAddress($scope.address)
						.then(function (result) {
							$ionicLoading.hide();
							console.log('save address promise is resolved', result);
							if (result.success) {
								$scope.user.currentAddress = result.response[0];
								UserService.storeInfo($scope.user);
								//vendor update for business area start here
								//$scope.vendors = $scope.vendorFromUserAddress(); //vendor updation is not required here it will happen when the instant modal is opened
								$scope.getDeliveryNow.addressId = $scope.user.currentAddress.id;
								$scope.closeModal(4);
								
								if (isServiceable) {
									//dynamicAlert.showAlert('you are reachable! Enjoy the Service');
									window.plugins.toast.showShortBottom(
										"Address saved!", function (a) { }, function (b) { }
									);
									$scope.openModal(2); //open instant bottle page after address is saved
								} else {
									//dynamicAlert.showAlert('Currently we are not available at your address. Stay Tuned');
									window.plugins.toast.showLongBottom(
										"Currently we are not reachable at your address. Stay Tuned", function (a) { }, function (b) { }
									);
								}
							} else {
								console.log('something not good while saving address', result);
							}
						}, function (error) {
							console.log('save address promise is rejected', error);
							$ionicLoading.hide();
						});
				}

			} else {
				console.log("Form is invalid");
				window.plugins.toast.showShortBottom(
					"Please provide all the required fields", function (a) { }, function (b) { }
				);

			}
		};

		$scope.updateAddress = function () {
			Address.setUpdateFlag(true);
			$scope.openModal(4);
			$scope.closeModal(2);//close the instant modal page in the background.
		};

		// loading map
		$scope.getMap = function () {
			$scope.showSaveBtn = false;
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loading</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			cordova.plugins.diagnostic.getLocationAuthorizationStatus(function (status) {
				console.log('getLocationAuthorizationStatus is ', status);
				switch (status) {
					case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
						console.log("Permission not requested");
						cordova.plugins.diagnostic.requestLocationAuthorization(function (status) {
							console.log('requestLocationAuthorization status is ', status);
							switch (status) {
								case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
								case cordova.plugins.diagnostic.permissionStatus.GRANTED:
								case cordova.plugins.diagnostic.permissionStatus.DENIED:
								case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
								case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
									console.log("Permission granted to use the LOCATION");
									$scope.requestMap();
									break;
							}
						}, function (error) {
							console.error("The following error occurred: " + error);
						}, cordova.plugins.diagnostic.locationAuthorizationMode.GRANTED_WHEN_IN_USE);
						break;
					case cordova.plugins.diagnostic.permissionStatus.DENIED:
						console.log("Permission denied");
						cordova.plugins.diagnostic.requestLocationAuthorization(function (status) {
							console.log('requestLocationAuthorization status is ', status);
							switch (status) {
								case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
								case cordova.plugins.diagnostic.permissionStatus.GRANTED:
								case cordova.plugins.diagnostic.permissionStatus.DENIED:
								case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
								case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
									console.log("Permission granted to use the LOCATION");
									$scope.requestMap();
									break;
							}
						}, function (error) {
							console.error("The following error occurred: " + error);
						}, cordova.plugins.diagnostic.locationAuthorizationMode.GRANTED_WHEN_IN_USE);
						// cordova.plugins.diagnostic.switchToLocationSettings();
						// $ionicLoading.hide();
						// setTimeout(function () {
						// 	$scope.closeModal(4);
						// }, 2000); //smooth closing effect of modal
						break;
					case cordova.plugins.diagnostic.permissionStatus.GRANTED:
						console.log("Permission granted always");
						$scope.requestMap();
						break;
					case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
						// case cordova.plugins.diagnostic.permissionStatus.NOT_DETERMINED:
						$scope.requestMap();
						break;
				}
				if (status == 'not_determined') {
					cordova.plugins.diagnostic.requestLocationAuthorization(function (status) {
						console.log('requestLocationAuthorization status is ', status);
						switch (status) {
							case cordova.plugins.diagnostic.permissionStatus.GRANTED_WHEN_IN_USE:
							case cordova.plugins.diagnostic.permissionStatus.GRANTED:
							case cordova.plugins.diagnostic.permissionStatus.DENIED:
							case cordova.plugins.diagnostic.permissionStatus.NOT_REQUESTED:
							case cordova.plugins.diagnostic.permissionStatus.DENIED_ALWAYS:
								console.log("Permission granted to use the LOCATION");
								$scope.requestMap();
								break;
						}
					}, function (error) {
						console.error("The following error occurred: " + error);
					}, cordova.plugins.diagnostic.locationAuthorizationMode.GRANTED_WHEN_IN_USE);
				}
			}, function (error) {
				console.error("The following error occurred: " + error);
			});

			$scope.requestMap = function () {
				cordova.plugins.diagnostic.isLocationAvailable(function (available) {
					console.log("Location is " + (available ? "available" : "not available"));
					if (!available) {
						cordova.plugins.diagnostic.switchToLocationSettings();
						$ionicLoading.hide();
						setTimeout(function () {
							$scope.closeModal(4);
						}, 2000); //smooth closing effect of modal
					} else {

						$scope.locationCtrl = {
							coordinates: null
						};
						var options = { timeout: 10000, enableHighAccuracy: true };

						$cordovaGeolocation.getCurrentPosition(options).then(function (position) {

							// geoCoder
							var geocoder = new google.maps.Geocoder();
							var latLng;
							var lat;
							var lng;

							function geocodePosition(pos) {
								document.getElementById('mapAdd').innerHTML = '';
								geocoder.geocode({
									latLng: pos
								}, function (responses) {
									if (responses && responses.length > 0) {
										console.log('inside the geocode response ', responses);
										// document.getElementById('mapPointerAdd').innerHTML = responses[0].formatted_address; //mapAdd
										document.getElementById('mapAdd').innerHTML = responses[0].formatted_address;
										$scope.address.locality = responses[0].formatted_address;
										if (responses[0].formatted_address) {
											$scope.showSaveBtn = true;
											$scope.$apply();
										} else {
											$scope.showSaveBtn = false;
										}
									} else {
										// document.getElementById('mapPointerAdd').innerHTML = 'Cannot determine address at this location.';
										//$scope.address.locality = 'Cannot determine address at this location.';
										console.log('can not get the address');
									}
								});
							}
							// check for update Address
							if (Address.getUpdateFlag()) {
								console.log('updateAddress get map lat ' + $scope.user.currentAddress.lat + 'get map lng ' + $scope.user.currentAddress.lng);
								//for users who's lat lng is NULL show the current address
								if (!$scope.user.currentAddress.lat || !$scope.user.currentAddress.lng) {
									latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
									$scope.address.lat = position.coords.latitude;
									$scope.address.lng = position.coords.longitude;
									console.log('lat is ' + $scope.address.lat + ' lng is ' + $scope.address.lng);
									geocodePosition(latLng);
								} else {
									latLng = new google.maps.LatLng(+$scope.user.currentAddress.lat, +$scope.user.currentAddress.lng);
									geocodePosition(latLng);
								}
							} else { //current address
								latLng = new google.maps.LatLng(position.coords.latitude, position.coords.longitude);
								$scope.address.lat = position.coords.latitude;
								$scope.address.lng = position.coords.longitude;
								console.log('lat is ' + $scope.address.lat + ' lng is ' + $scope.address.lng);
								geocodePosition(latLng);
							}

							var mapOptions = {
								center: latLng,
								zoom: 17,
								mapTypeId: google.maps.MapTypeId.ROADMAP,
								disableDefaultUI: true,
								mapTypeControl: false,
								streetViewControl: false,
								draggable: true,
								//gestureHandling: 'cooperative'
							};

							$scope.map = new google.maps.Map(document.getElementById("map"), mapOptions);
							$scope.locationCtrl.coordinates = $scope.map.getCenter().toUrlValue();
							console.log('the cordinates are ', $scope.locationCtrl);
							var GeoMarker = new GeolocationMarker($scope.map);
							var handleMapEvent = function () {
								$ionicLoading.show();
								$scope.locationCtrl.coordinates = $scope.map.getCenter().toUrlValue();
								// $scope.$apply();
								console.log('the cordinates are ', $scope.locationCtrl);
								$scope.address.lat = +$scope.locationCtrl.coordinates.split(',')[0];
								$scope.address.lng = +$scope.locationCtrl.coordinates.split(',')[1];
								latLng = new google.maps.LatLng($scope.address.lat, $scope.address.lng);
								geocodePosition(latLng);
								$scope.$apply();
								$ionicLoading.hide();
							};
							// google.maps.event.addListener($scope.map, "dragend", function () {
							//  console.log('center_changed event triggered');
							//  handleMapEvent();
							// });
							// google.maps.event.addListener($scope.map, "center_changed", function () {
							//  console.log('center_changed event triggered');
							//  handleMapEvent();
							// });
							google.maps.event.addListener($scope.map, 'idle', function () {
								// do something only the first time the map is loaded
								handleMapEvent();
							});

						}, function (error) {
							console.log("Could not get location. Make sure your GPS is ON");
							window.plugins.toast.showLongTop(
								"Could not get location. Make sure your GPS is ON or Re-try again", function (a) { }, function (b) { }
							);
							$ionicLoading.hide();
						});
					}
				}, function (error) {
					console.error("The following error occurred: " + error);
				});
			};
		}; //end of getMap()

		//place orders and deliveries

		$scope.confirmDelivery = function () {

			var dateDelivery = new Date();
			var hourDelivery = dateDelivery.getHours();
			var minutesDelivery = dateDelivery.getMinutes();
			if (hourDelivery >= 19 || (hourDelivery == 18 && minutesDelivery >= 30)) {
				var nextHoliday = new Date($scope.user.currentAddress.businessArea.nextHoliday);
				if(nextHoliday.getDate() == dateDelivery.getDate()+1 && nextHoliday.getMonth() == dateDelivery.getMonth() && nextHoliday.getFullYear() == dateDelivery.getFullYear()){
					window.plugins.toast.showShortBottom(
						"Tomorrow is holiday", function (a) { }, function (b) { }
					);
					return false;
				}
			}
			if (!$scope.user.currentAddress) {
				window.plugins.toast.showShortBottom(
					"Please provide the Delivery Address", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedDelivery) {
				window.plugins.toast.showShortBottom(
					"Please choose Delivery Type", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedSlot) {
				window.plugins.toast.showShortBottom(
					"Please choose your Delivery Slot", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.user.currentAddress.isServiceable) {
				//continue to place the COD order Check User time with server time and isServiceable for the given address
				window.plugins.toast.showLongBottom(
					"Currently we are not reachable at your address. Stay Tuned", function (a) { }, function (b) { }
				);
				return false;
			}
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Placing order</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			UserService.serverTime().then(function (result) {
				if (result.data.success) {
					var userTime = new Date();
					console.log("the user time is ", userTime);
					var userHour = userTime.getHours();
					var userDay = userTime.getDay();
					var userMin = userTime.getMinutes();
					var serverTime = new Date(+result.data.response);
					var serverHour = serverTime.getHours();
					var serverDay = serverTime.getDay();
					var serverMonth = serverTime.getMonth();
					var serverMin = serverTime.getMinutes()
					console.log("the server time is ", serverTime);
					if (serverHour === userHour && serverDay === userDay && serverMonth === userTime.getMonth()) { //&& serverMin === userMin

						var order = {
							vendorId: $scope.selectedVendor.id,
							productId: 'none',//$scope.selectedVendor.products[0].id,//'none', //placing delivery of wallet bottles
							bottles: $scope.getDeliveryNow.numberOfBottles,
							delivery_type: $scope.selectedDelivery.type,
							payment_type: 'none',
							slot_id: $scope.selectedSlot.id,
							is_exchange: $scope.exchange.bottle,
							addressId: $scope.user.currentAddress.id
						};

						Payment.placeOrder(order)
							.then(function (result) {
								console.log('placeOrder promise is resolved', result);
								$ionicLoading.hide();
								if (result.success) {
									$scope.user = $rootScope.prepareUserData(result.response.customer);
									$scope.closeModal(2);
									window.plugins.toast.showShortBottom(
										"Order placed successfuly", function (a) { }, function (b) { }
									);

									//for localNotification upcoming release
									// if (order.delivery_type == "SCHEDULE") {
									// 	if ($scope.user.firstname) {
									// 		var usr = 'Dear ' + $scope.user.firstname + ', Take Action!'
									// 	} else {
									// 		var usr = 'Dear Customer, Take Action!'
									// 	}
									// 	scheduleDeliveryNotification(order.slot_id, '', result.response.id, usr);
									// }
								} else {
									window.plugins.toast.showShortBottom(
										"Order could not placed", function (a) { }, function (b) { }
									);
								}
							}, function (error) {
								console.log('placeOrder promise is rejected', error);
								$ionicLoading.hide();
							});
					} else {
						$ionicLoading.hide();
						window.plugins.toast.showShortBottom(
							"Please check your device time!", function (a) { }, function (b) { }
						);
						return false;
					}
				} else {
					$ionicLoading.hide();
					console.log('ServerTime promise something went wrong');
				}
			}, function (error) {
				$ionicLoading.hide();
				console.log('ServerTime promise is rejected', error);
			});

		};

		$scope.payNow = function () {
			var dateDelivery = new Date();
			var hourDelivery = dateDelivery.getHours();
			var minutesDelivery = dateDelivery.getMinutes();
			if (hourDelivery >= 19 || (hourDelivery == 18 && minutesDelivery >= 30)) {
				var nextHoliday = new Date($scope.user.currentAddress.businessArea.nextHoliday);
				if(nextHoliday.getDate() == dateDelivery.getDate()+1 && nextHoliday.getMonth() == dateDelivery.getMonth() && nextHoliday.getFullYear() == dateDelivery.getFullYear()){
					window.plugins.toast.showShortBottom(
						"Tomorrow is holiday", function (a) { }, function (b) { }
					);
					return false;
				}
			}
			if (!$scope.user.currentAddress) {
				window.plugins.toast.showShortBottom(
					"Please provide the Delivery Address", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedDelivery) {
				window.plugins.toast.showShortBottom(
					"Please choose Delivery Type", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedSlot) {
				window.plugins.toast.showShortBottom(
					"Please choose your Delivery Slot", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.user.currentAddress.isServiceable) {
				//continue to place the COD order Check User time with server time and isServiceable for the given address
				window.plugins.toast.showLongBottom(
					"Currently we are not reachable at your address. Stay Tuned", function (a) { }, function (b) { }
				);
				return false;
			}
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Proceed to payment</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			UserService.serverTime().then(function (result) {
				if (result.data.success) {
					var userTime = new Date();
					console.log("the user time is ", userTime);
					var userHour = userTime.getHours();
					var userDay = userTime.getDay();
					var userMin = userTime.getMinutes();
					var serverTime = new Date(+result.data.response);
					var serverHour = serverTime.getHours();
					var serverDay = serverTime.getDay();
					var serverMonth = serverTime.getMonth();
					var serverMin = serverTime.getMinutes()
					console.log("the server time is ", serverTime);
					if (serverHour === userHour && serverDay === userDay && serverMonth === userTime.getMonth()) { //&& serverMin === userMin

						if (! +$scope.user.walletBottles) { 
							var order = {
								vendorId: $scope.selectedVendor.id,
								productId: $scope.selectedVendor.products[0].id,
								bottles: $scope.getDeliveryNow.numberOfBottles, 
								delivery_type: $scope.selectedDelivery.type,
								payment_type: 'ONLINE',
								slot_id: $scope.selectedSlot.id,
								is_exchange: $scope.exchange.bottle,
								addressId: $scope.user.currentAddress.id
							};
						} else {
							var order = {
								vendorId: $scope.selectedVendor.id,
								productId: $scope.selectedVendor.products[0].id,//'none', //placing delivery of wallet bottle (sign up case)
								bottles: $scope.getDeliveryNow.numberOfBottles, //according to the condition it will be 1 always
								delivery_type: $scope.selectedDelivery.type,
								payment_type: 'ONLINE',
								slot_id: $scope.selectedSlot.id,
								is_exchange: $scope.exchange.bottle,
								addressId: $scope.user.currentAddress.id
							};
						}

						Payment.placeOrder(order)
							.then(function (res) {
								$ionicLoading.hide();
								console.log('addBottles promise is resolved ', res);
								var orderBottleRes = res;
								if (res.success) {
									if (!res.response.customer.emailId) {
										res.response.customer.emailId = 'support@eazybottle.com';
									}
									var callbackStringFormat = PAYTM.CALLBACK_URL.replace(/%s/i, res.response.id);
									var params = {
										//res.response.id, res.response.customer.phone, res.response.customer.emailId, res.response.customer.phone, res.response.amount + res.response.depositAmt,
										INDUSTRY_TYPE_ID: PAYTM.INDUSTRY_TYPE_ID,
										CHANNEL_ID: PAYTM.CHANNEL_ID,
										MOBILE_NO: res.response.customer.phone,
										REQUEST_TYPE: PAYTM.REQUEST_TYPE,
										TXN_AMOUNT: res.response.amount + res.response.depositAmt,
										MID: PAYTM.MID,
										EMAIL: res.response.customer.emailId,
										THEME: PAYTM.THEME,
										CALLBACK_URL: callbackStringFormat,
										CUST_ID: res.response.customer.phone,
										WEBSITE: PAYTM.WEBSITE,
										ORDER_ID: res.response.id
									};
									$ionicLoading.show({
										template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loading</p>',
										animation: 'fade-in',
										showBackdrop: true,
										showDelay: 0
									});
									Payment.generateChecksum(params)
										.then(function (chsm) {
											console.log('checksum generation is success', chsm);
											window.plugins.paytm.startPayment(chsm.data.ORDER_ID, chsm.data.CUST_ID, chsm.data.EMAIL, chsm.data.MOBILE_NO, chsm.data.TXN_AMOUNT, chsm.data.CHECKSUMHASH, paymentSuccess, paymentfailure);
											function paymentSuccess(result) {
												console.log('online order status is ', result);
												var pgsdkRES = result;
												Payment.reVerify(pgsdkRES)
													.then(function (reverifyPASS) {
														console.log('reverify success callback', reverifyPASS);
														$ionicLoading.hide();
														if (reverifyPASS.data.success && reverifyPASS.data.response.order_STATUS === "PLACED") {
															$scope.refreshUserInfo();
															$scope.closeModal(2);
															window.plugins.toast.showLongTop(
																"Order placed successfuly!", function (a) { }, function (b) { }
															);
														} else {
															window.plugins.toast.showLongTop(
																"Payment did not succeed!", function (a) { }, function (b) { }
															);
														}
													}, function (reverifyERROR) {
														console.log('reverify error callback', reverifyERROR);
														$ionicLoading.hide();
														window.plugins.toast.showLongTop(
															"Payment did not succeed!", function (a) { }, function (b) { }
														);
													});
											};
											function paymentfailure(err) {
												Payment.paymentFailure({ orderId: orderBottleRes.response.id }).then(successCallback, errorCallback);
												function successCallback(res) {
													$ionicLoading.hide();
													if (res.data.success) {
														console.log('failed status has been updated', res);
														window.plugins.toast.showShortBottom(
															"Order cancelled!", function (a) { }, function (b) { }
														);
													} else {
														console.log('failed status', res);
														window.plugins.toast.showShortBottom(
															"Order cancelled!", function (a) { }, function (b) { }
														); //should be removed later
													}
												};
												function errorCallback(res) {
													console.log('failed status did not reported', res);
												}
											};
										}, function (error) {
											console.log('checksum generation failed', error);
										});
								}
							}, function (error) {
								$ionicLoading.hide();
								console.log('add bottles promise rejected ', res);
							});
					} else {
						$ionicLoading.hide();
						window.plugins.toast.showShortBottom(
							"Please check your device time!", function (a) { }, function (b) { }
						);
						return false;
					}
				} else {
					$ionicLoading.hide();
					console.log('ServerTime promise something went wrong');
				}
			}, function (error) {
				$ionicLoading.hide();
				console.log('ServerTime promise is rejected', error);
			});

		};

		$scope.cashOnDelivery = function () {
			var dateDelivery = new Date();
			var hourDelivery = dateDelivery.getHours();
			var minutesDelivery = dateDelivery.getMinutes();
			if (hourDelivery >= 19 || (hourDelivery == 18 && minutesDelivery >= 30)) {
				var nextHoliday = new Date($scope.user.currentAddress.businessArea.nextHoliday);
				if(nextHoliday.getDate() == dateDelivery.getDate()+1 && nextHoliday.getMonth() == dateDelivery.getMonth() && nextHoliday.getFullYear() == dateDelivery.getFullYear()){
					window.plugins.toast.showShortBottom(
						"Tomorrow is holiday", function (a) { }, function (b) { }
					);
					return false;
				}
			}
			if (!$scope.user.currentAddress) {
				window.plugins.toast.showShortBottom(
					"Please provide the Delivery Address", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedDelivery) {
				window.plugins.toast.showShortBottom(
					"Please choose Delivery Type", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedSlot) {
				window.plugins.toast.showShortBottom(
					"Please choose your Delivery Slot", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.user.currentAddress.isServiceable) {
				//continue to place the COD order Check User time with server time and isServiceable for the given address
				window.plugins.toast.showLongBottom(
					"Currently we are not reachable at your address. Stay Tuned!", function (a) { }, function (b) { }
				);
				return false;
			}
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Please Confirm</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			UserService.serverTime().then(function (result) {
				if (result.data.success) {
					var userTime = new Date();
					console.log("the user time is ", userTime);
					var userHour = userTime.getHours();
					var userDay = userTime.getDay();
					var userMin = userTime.getMinutes();
					var serverTime = new Date(+result.data.response);
					var serverHour = serverTime.getHours();
					var serverDay = serverTime.getDay();
					var serverMonth = serverTime.getMonth();
					var serverMin = serverTime.getMinutes()
					console.log("the server time is ", serverTime);
					if (serverHour === userHour && serverDay === userDay && serverMonth === userTime.getMonth()) { //&& serverMin === userMin
						if (! +$scope.user.walletBottles) { //user wallet is ZERO
							var order = {
								vendorId: $scope.selectedVendor.id,
								productId: $scope.selectedVendor.products[0].id,
								bottles: $scope.getDeliveryNow.numberOfBottles,
								delivery_type: $scope.selectedDelivery.type,
								payment_type: 'COD',
								slot_id: $scope.selectedSlot.id,
								is_exchange: $scope.exchange.bottle,
								addressId: $scope.user.currentAddress.id
							};
						} else {
							var order = {
								vendorId: $scope.selectedVendor.id,
								productId: $scope.selectedVendor.products[0].id,//'none', //placing delivery of wallet bottle (sign up case)
								bottles: $scope.getDeliveryNow.numberOfBottles,
								delivery_type: $scope.selectedDelivery.type,
								payment_type: 'COD',
								slot_id: $scope.selectedSlot.id,
								is_exchange: $scope.exchange.bottle,
								addressId: $scope.user.currentAddress.id
							};
						}
						$ionicLoading.hide(); //please confirm loading
						var cancelPopUp = $ionicPopup.show({
							title: 'Confirm Cash on Delivery order',
							scope: $scope,
							buttons: [{
								text: 'Cancel',
								type: 'button-later',
								onTap: function (e) {
									cancelPopUp.close();
								}
							},
							{
								text: '<b>Confirm</b>',
								type: 'button-get-now',
								onTap: function (e) {
									$ionicLoading.show({
										template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Placing order</p>',
										animation: 'fade-in',
										showBackdrop: true,
										showDelay: 0
									});

									Payment.placeOrder(order)
										.then(function (result) {
											console.log('placeOrder promise is resolved', result);
											$ionicLoading.hide();
											if (result.success) {
												$scope.user = $rootScope.prepareUserData(result.response.customer);
												$scope.closeModal(2);
												window.plugins.toast.showShortBottom(
													"Order placed successfuly", function (a) { }, function (b) { }
												);
											} else {
												window.plugins.toast.showShortBottom(
													"Order could not place. Please try again!", function (a) { }, function (b) { }
												);
											}
										}, function (error) {
											console.log('placeOrder promise is rejected', error);
											$ionicLoading.hide();
										});
								}
							}]
						});
					} else {
						$ionicLoading.hide();
						window.plugins.toast.showShortBottom(
							"Please check your device time!", function (a) { }, function (b) { }
						);
						return false;
					}
				} else {
					$ionicLoading.hide();
					console.log('ServerTime promise something went wrong');
				}
			}, function (error) {
				$ionicLoading.hide();
				console.log('ServerTime promise is rejected', error);
			});
		};

		// wallet Purchase
		$scope.walletPurchase = function () {
			if (!$scope.selectedProduct) {
				window.plugins.toast.showShortBottom(
					"Please choose Offer!", function (a) { }, function (b) { }
				);
				return false;
			}
			if (!$scope.selectedVendor) {
				window.plugins.toast.showShortBottom(
					"Please choose Vendor!", function (a) { }, function (b) { }
				);
				return false;
			}
			if ($scope.selectedProduct.type == "SPECIAL" && $scope.user.offerAvailed) {
				window.plugins.toast.showLongBottom(
					"You can avail this offer only once per Customer", function (a) { }, function (b) { }
				);
				return false;
			}
			var refundStatus = '';
			$scope.user.bottles.forEach(function (obj) {
				if (obj.vendor.id == $scope.selectedVendor.id) {
					refundStatus = obj.refundStatus;
				}
			});
			if (refundStatus == "INITIATED") {
				window.plugins.toast.showLongTop(
					"You've Requested for Bottle Refund!", function (a) { }, function (b) { }
				);
				return false;
			}
			var order = {
				productId: $scope.selectedVendor.products[0].id,
				vendorId: $scope.selectedVendor.id,
				offerId: $scope.selectedProduct.id
				//is_exchange: false //not required for the wallet purchse
			};
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loading</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			Payment.addBottles(order)
				.then(function (res) {
					$ionicLoading.hide();
					console.log('addBottles promise is resolved ', res);
					var addBottleRes = res;
					if (res.data.success) {
						if (!res.data.response.customer.emailId) {
							res.data.response.customer.emailId = 'support@eazybottle.com';
						}
						var callbackStringFormat = PAYTM.CALLBACK_URL.replace(/%s/i, res.data.response.id);
						var params = {
							//res.response.id, res.response.customer.phone, res.response.customer.emailId, res.response.customer.phone, res.response.amount + res.response.depositAmt,
							INDUSTRY_TYPE_ID: PAYTM.INDUSTRY_TYPE_ID,
							CHANNEL_ID: PAYTM.CHANNEL_ID,
							MOBILE_NO: res.data.response.customer.phone,
							REQUEST_TYPE: PAYTM.REQUEST_TYPE,
							TXN_AMOUNT: res.data.response.amount + res.data.response.depositAmt,
							MID: PAYTM.MID,
							EMAIL: res.data.response.customer.emailId,
							THEME: PAYTM.THEME,
							CALLBACK_URL: callbackStringFormat,
							CUST_ID: res.data.response.customer.phone,
							WEBSITE: PAYTM.WEBSITE,
							ORDER_ID: res.data.response.id
						};
						$ionicLoading.show({
							template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Loading</p>',
							animation: 'fade-in',
							showBackdrop: true,
							showDelay: 0
						});
						Payment.generateChecksum(params)
							.then(function (chsm) {
								console.log('generate checksum success', chsm)
								window.plugins.paytm.startPayment(chsm.data.ORDER_ID, chsm.data.CUST_ID, chsm.data.EMAIL, chsm.data.MOBILE_NO, chsm.data.TXN_AMOUNT, chsm.data.CHECKSUMHASH, paymentSuccess, paymentfailure);
								function paymentSuccess(result) {
									console.log('addBottle order status is before json parse', result);
									var pgsdkRES = result;
									console.log('addBottle order status is ', pgsdkRES);
									//re-verify the status with EazyBottle server.
									Payment.reVerify(pgsdkRES)
										.then(function (reverifyPASS) {
											console.log('reverify success callback', reverifyPASS);
											$ionicLoading.hide();
											if (reverifyPASS.data.success && reverifyPASS.data.response.order_STATUS === "PLACED") {
												$scope.refreshUserInfo();
												$scope.closeModal(1);
												window.plugins.toast.showLongTop(
													"Hurray! Bottles added to your Wallet!", function (a) { }, function (b) { }
												);
											} else {
												window.plugins.toast.showLongTop(
													"Payment did not succeed!", function (a) { }, function (b) { }
												);
											}
										}, function (reverifyERROR) {
											console.log('reverify error callback', reverifyERROR);
											$ionicLoading.hide();
											window.plugins.toast.showLongTop(
												"Payment did not succeed!", function (a) { }, function (b) { }
											);
										});
								};
								function paymentfailure(err) {
									Payment.paymentFailure({ orderId: addBottleRes.data.response.id }).then(successCallback, errorCallback);
									function successCallback(res) {
										$ionicLoading.hide();
										if (res.data.success) {
											console.log('failed status has been updated', res);
											window.plugins.toast.showShortBottom(
												"Order cancelled!", function (a) { }, function (b) { }
											);
										}
									};
									function errorCallback(res) {
										$ionicLoading.hide();
										console.log('failed status did not reported', res);
									}
								};
							}, function (error) { });
					}
				}, function (error) {
					$ionicLoading.hide();
					console.log('add bottles promise rejected ', res);
				});

		};


		//auto delivery modal functionality starts here

		//over riding the date ISO method
		Date.prototype.toLocalISOString = function () {

			// Helper for padding
			function pad(n, len) {
				return ('000' + n).slice(-len);
			}

			// If not called on a Date instance, or timevalue is NaN, return undefined
			if (isNaN(this) || Object.prototype.toString.call(this) != '[object Date]') return;

			// Otherwise, return an ISO format string with the current system timezone offset
			var d = this;
			var os = d.getTimezoneOffset();
			var sign = (os > 0 ? '-' : '+');
			os = Math.abs(os);

			return pad(d.getFullYear(), 4) + '-' +
				pad(d.getMonth() + 1, 2) + '-' +
				pad(d.getDate(), 2) +
				'T' +
				pad(d.getHours(), 2) + ':' +
				pad(d.getMinutes(), 2) + ':' +
				pad(d.getSeconds(), 2) + '.' +
				pad(d.getMilliseconds(), 3) +

				// Note sign of ECMASCript offsets are opposite to ISO 8601
				sign +
				pad(os / 60 | 0, 2) + ':' +
				pad(os % 60, 2);
		};

		$scope.today = new Date();
		$scope.today.setDate($scope.today.getDate() + 1);
		$scope.tomorrow = $scope.today.toLocalISOString().split('T')[0];

		$scope.auto = function () {
			if (! +$scope.user.walletBottles || $scope.user.newUser) { //for newUser and zero wallet bottles
				window.plugins.toast.showShortTop(
					"Bottle Balance is low", function (a) { }, function (b) { }
				);
				$scope.isAuto.selection = $scope.user.isAutoDelivery;
				return false;
			}
			if ($scope.user.isAutoDelivery) {//true
				console.log('the isauto.selection value is ', $scope.isAuto.selection);
				var cancelPopUp = $ionicPopup.show({
					title: 'Disable Auto Delivery?',
					scope: $scope,
					buttons: [{
						text: 'No',
						type: 'button-later',
						onTap: function (e) {
							cancelPopUp.close();
							$scope.isAuto.selection = $scope.user.isAutoDelivery;
							// $scope.autoMsg = 'is ON';
						}
					},
					{
						text: '<b>Yes</b>',
						type: 'button-get-now',
						onTap: function (e) {
							Slots.setAutoDelivery({ isauto: $scope.isAuto.selection })
								.then(function (result) {
									console.log('setAutoDelivery promise is resolved', result);
									if (result.success) {
										$scope.user = $rootScope.prepareUserData(result.response);
										$scope.isAuto.selection = $scope.user.isAutoDelivery;
										$scope.upcomingMessage = "Use Auto Delivery";
										window.plugins.toast.showShortTop(
											"Auto Delivery disabled!", function (a) { }, function (b) { }
										);
										cancelDeliveryNotification(1);
									} else {
										console.log('setAutoDelivery promise something went wrong');
									}
								}, function (error) {
									console.log('setAutoDelivery promise is rejected', error);
								});
						}
					}
					]
				});
			} else {
				if (!$scope.selectedAutoSlot) {
					window.plugins.toast.showShortTop("Please choose Slot", function (a) { }, function (b) { });
					$scope.isAuto.selection = false;
					return false;
				}
				if (!$scope.selectedDay) {
					window.plugins.toast.showShortTop("Please select Days", function (a) { }, function (b) { });
					$scope.isAuto.selection = false;
					return false;
				}

				Slots.setAutoDelivery({ isauto: $scope.isAuto.selection })
					.then(function (result) {
						console.log('setAutoDelivery promise is resolved', result);
						if (result.success) {
							$scope.user = $rootScope.prepareUserData(result.response);
							$scope.isAuto.selection = $scope.user.isAutoDelivery;
							// update the upcomingMessage here
							if ($scope.user.depositAmt && +$scope.user.walletBottles) {
								$scope.updateUpcomingDate($scope.user.scheduledTime);
							}
							window.plugins.toast.showShortTop(
								"Auto Delivery enabled!", function (a) { }, function (b) { }
							);
							//for localNotification upcoming release
							// if ($scope.user.firstname) {
							// 	var usr = 'Dear ' + $scope.user.firstname + ', Take Action!'
							// } else {
							// 	var usr = 'Dear Customer, Take Action!'
							// }
							// scheduleDeliveryNotification(1, '', result.response.id, usr);
						} else {
							console.log('setAutoDelivery promise something went wrong');
						}
					}, function (error) {
						console.log('setAutoDelivery promise is rejected', error);
					});

			}
		};
		$scope.changeDate = function () {
			console.log('the modified date is ', $scope.schedule.date);
			Slots.setAutoUpcomingDelivery({ scheduled_date: Date.parse($scope.schedule.date) })
				.then(function (result) {
					console.log('setAutoUpcomingDelivery promise is resolved', result);
					if (result.success) {
						$scope.user = $rootScope.prepareUserData(result.response);
						$scope.updateUpcomingDate($scope.user.scheduledTime);
						window.plugins.toast.showShortTop(
							"Upcoming Delivery updated!", function (a) { }, function (b) { }
						);
					} else {
						console.log('setAutoUpcomingDelivery promise something went wrong');
					}
				}, function (error) {
					console.log('setAutoUpcomingDelivery promise is rejected', error);
				});
		};

		$rootScope.prepareUserData = function (usr) {
			var bottleCount = UserService.totalBottles(usr.bottles);
			var depositArray = [];
			usr.isRefund = 0; //it's always false set by client app
			usr.walletBottles = bottleCount;
			if(usr.bottles){
				usr.bottles.map(function (obj) {
					if(obj.refundStatus == 'NONE'){
						depositArray.push(obj.depositAmt);
					}
					obj.vendor.url = SERVER.URL + obj.vendor.url;
					obj.vendor.reports.map(function (o) {
						o.url = SERVER.URL + o.url;
						return o;
					});
					return obj;
				});
				if(depositArray.length){
					usr.depositAmt = depositArray.reduce(function(sum, value) {
						return sum + value;
					}, 0); //user has paid deposit for one of the vendor, add all of the deposit and display/store
				} else{
					usr.depositAmt = 0; //user hasn't paid deposit for any of the vendor
				}
			} else{
				user.depositAmt = 0;
			}
			//$scope.user = usr;
			UserService.storeInfo(usr);
			return usr;
		};

		$scope.cancelRefund = function (id) {
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Unlocking</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			Order.cancelBottleRefund({ bottleId: id }).then(cancelSucces, cancelError);
			function cancelSucces(res) {
				$ionicLoading.hide();
				console.log("cancelRefund promise resolved", res);
				if (res.success) {
					$scope.user = $rootScope.prepareUserData(res.response.customer);
					//$scope.getDeliveryNow.deposit = 0; //deposit set to zero back on modal-instant page for total calculation
					$scope.select_vendor($scope.vendors[0].id);
					window.plugins.toast.showShortBottom(
						"Enjoy the service!", function (a) { }, function (b) { }
					);
				} else {
					console.log("cancelRefund promise gone wrong", res);
					$ionicLoading.hide();
				}
			};
			function cancelError(error) {
				console.log("cancelRefund promise is rejected", error);
			};
		};

	})

	.controller('OrdersCtrl', function ($rootScope, $scope, $ionicModal, $ionicPopup, $ionicLoading, Order) {

		$scope.orderList = Order.fetchOrderList();
		$scope.deliveryList = Order.fetchDeliveryList();
		$scope.monthNames = ["January", "February", "March", "April", "May", "June",
			"July", "August", "September", "October", "November", "December"
		];

		$scope.noOrders = false;
		$scope.showDeliveryCard = true;
		$scope.deliveryColor = 'color:#7fcdff';
		$scope.orderColor = 'color:#000000';
		$scope.viewName = 'Deliveries';

		$scope.showOrders = function () {
			$scope.showDeliveryCard = false;
			$scope.showOrderCard = true;
			$scope.orderColor = 'color:#7fcdff';
			$scope.deliveryColor = 'color:#000000';
			$scope.viewName = 'Transactions';
		};
		$scope.showDeliveries = function () {
			$scope.showOrderCard = false;
			$scope.showDeliveryCard = true;
			// add style to the text highlights
			$scope.deliveryColor = 'color:#7fcdff';
			$scope.orderColor = 'color:#000000'
			$scope.viewName = 'Deliveries';
		};

		$scope.$on('$ionicView.enter', function (e) {
			$scope.refreshDeliveries();
			$scope.refreshOrders();
		});
		$scope.refreshDeliveries = function () {
			Order.getDelivery()
				.then(function (result) {
					console.log('delivery promise is resolved', result);
					if (result.success) {
						result.response.forEach(function (elm) {
							var deliveryTime = elm.deliveryTime;
							var hour = new Date(deliveryTime).getHours();
							var minutes = new Date(deliveryTime).getMinutes();
							if (hour.toString().length == 1) {
								hour = "0" + hour;
							}
							if (minutes.toString().length == 1) {
								minutes = "0" + minutes;
							}
							elm.deliveryTime = hour + ':' + minutes + ' ' + $scope.monthNames[new Date(deliveryTime).getMonth()] + ' ' + new Date(deliveryTime).getDate() + ', ' + new Date(deliveryTime).getFullYear();
							var dateModified = new Date(elm.lastModified);
							elm.lastModified = $scope.monthNames[dateModified.getMonth()] + ' ' + dateModified.getDate() + ', ' + dateModified.getFullYear();
						})
						$scope.deliveryList = result.response;
						Order.storeDeliveryList(result.response);
					} else {
						console.log('delivery promise something went wrong');
					}
				}, function (error) {
					console.log('delivery promise is rejected', error);
				});
		};

		$scope.refreshOrders = function () {
			Order.getOrders()
				.then(function (result) {
					console.log('Order promise is resolved', result);
					if (result.success) {
						result.response.forEach(function (elm) {
							var dateModified = new Date(elm.createdDate);
							elm.createdDate = $scope.monthNames[dateModified.getMonth()] + ' ' + dateModified.getDate() + ', ' + dateModified.getFullYear();
							elm.amount = elm.amount + elm.depositAmt;
						})
						$scope.orderList = result.response;
						Order.storeOrderList(result.response);
					} else {
						console.log('Order promise something went wrong');
					}
				}, function (error) {
					console.log('Order promise is rejected', error);
				});
		};

		$scope.deliveryDetail = function (id) {
			console.log('delivery ID to be detailed is ', id);
			$scope.deliveryInfo = $scope.deliveryList.find(function (obj) {
				if (obj.id == id) {
					return obj;
				}
			});
			$scope.openModal(1);//modal-delivery-detail.html
		};

		$scope.orderDetail = function (id) {
			console.log('order ID to be detailed is ', id);
			$scope.data = $scope.orderList.find(function (obj) {
				if (obj.id == id) {
					return obj;
				}
			});
			$scope.openModal(2);//modal-order-detail.html
		};

		$scope.track = function () {
			console.log('track the delivery');
			var cancelPopUp = $ionicPopup.show({
				title: 'Phone Delivery Manager?',
				scope: $scope,
				buttons: [{
					text: 'No',
					type: 'button-later',
					onTap: function (e) {
						cancelPopUp.close();
					}
				},
				{
					text: '<b>Call</b>',
					type: 'button-get-now',
					onTap: function (e) {
						//open the dialer
						document.location.href = "tel:+917204661598";
					}
				}]
			});
		};

		$scope.cancel = function (delId) {
			// show an alert before cancel

			var cancelPopUp = $ionicPopup.show({
				title: 'Cancel the Delivery?',
				scope: $scope,
				buttons: [{
					text: 'No',
					type: 'button-later',
					onTap: function (e) {
						cancelPopUp.close();
					}
				},
				{
					text: '<b>Yes</b>',
					type: 'button-get-now',
					onTap: function (e) {
						$ionicLoading.show({
							template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Cancelling..</p>',
							animation: 'fade-in',
							showBackdrop: true,
							showDelay: 0
						});
						Order.cancelDelivery({ id: delId })
							.then(function (result) {
								console.log('cancelDelivery promise is resolved', result);
								$ionicLoading.hide();
								if (result.success) {
									var canceledDel = $scope.deliveryList.findIndex(function (value) {
										return value.id === delId;
									});
									$scope.deliveryList[canceledDel].status = result.response.status;
									Order.storeDeliveryList($scope.deliveryList);
									$scope.user = $rootScope.prepareUserData(result.response.customer);
								} else {
									console.log('cancelDelivery promise something went wrong');
								}
							}, function (error) {
								console.log('cancelDelivery promise is rejected', error);
							});
					}
				}]
			});
		};

		$ionicModal.fromTemplateUrl('templates/modal-delivery-detail.html', {
			id: '1',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal1 = modal;
		});
		//imageView fullscreen
		$ionicModal.fromTemplateUrl('templates/modal-order-detail.html', {
			id: '2',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal2 = modal;
		});
		$scope.openModal = function (index) {
			if (index == 1) $scope.oModal1.show();
			else if (index == 2) $scope.oModal2.show();
		};
		$scope.closeModal = function (index) {
			if (index == 1) $scope.oModal1.hide();
			else if (index == 2) $scope.oModal2.hide();
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function () {
			console.log('Destroying modals...');
			$scope.oModal1.remove();
			$scope.oModal2.remove();
		}); //modal end here

	})

	.controller('AccountCtrl', function ($rootScope, $scope, $state, $ionicModal, $ionicScrollDelegate, $ionicPopup, $ionicLoading, $ionicHistory, $timeout, Order, UserService, Address) {

		//$scope.vendors = [{ id: 1, name: 'Pearl Drops', image: 'img/nandu.png', price: 40 }];//replace this array with user bottles array for bottles count --> this is applicable for My Vendor also
		$scope.user = UserService.fetchUserInfo();
		$scope.businessValues = Address.fetchBusinessValues();
		$scope.supportNumber = "tel:" + $scope.businessValues.supportNumber;
		$scope.$on('$ionicView.enter', function (e) {
			$scope.user = UserService.fetchUserInfo();
			//showing the vendors for customer wallet bottles [this is for multiple vendor bottles use case]
			var walletVendor = [];
			$scope.user.bottles.forEach(function (obj) {
				walletVendor.push(obj.vendor);
			});
			$scope.vendors = walletVendor;
		});
		// appVersion from the plugin
		$scope.appVersion = '';
		cordova.getAppVersion(function (version) {
			$scope.appVersion = version;
		});

		if (!$scope.user.firstname && !$scope.user.lastname && !$scope.user.emailId) {
			$scope.usr = {
				firstname: '',
				lastname: '',
				email: ''
			};
			$scope.headline = "ADD YOUR NAME";
		} else {
			$scope.usr = {
				firstname: $scope.user.firstname,
				lastname: $scope.user.lastname,
				email: $scope.user.emailId
			};
			if (!$scope.user.lastname) {
				$scope.headline = $scope.user.firstname;
			} else {
				$scope.headline = $scope.user.firstname + ' ' + $scope.user.lastname;
			}
		}

		//about modal page
		$ionicModal.fromTemplateUrl('templates/modal-about.html', {
			id: '1',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal1 = modal;
		});
		//my-vendor modal page
		$ionicModal.fromTemplateUrl('templates/modal-vendor.html', {
			id: '2',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal2 = modal;
		});
		//deposit modal page
		$ionicModal.fromTemplateUrl('templates/modal-deposit.html', {
			id: '3',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal3 = modal;
		});
		//edit account info Modal page
		$ionicModal.fromTemplateUrl('templates/modal-account.html', {
			id: '4',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal4 = modal;
		});
		//imageView fullscreen
		$ionicModal.fromTemplateUrl('templates/modal-imageView.html', {
			id: '5',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal5 = modal;
		});
		//FAQ modal page
		$ionicModal.fromTemplateUrl('templates/modal-FAQ.html', {
			id: '6',
			scope: $scope,
			animation: 'slide-in-right'
		}).then(function (modal) {
			$scope.oModal6 = modal;
		});

		$scope.openModal = function (index) {
			if (index == 1) $scope.oModal1.show();
			else if (index == 2) $scope.oModal2.show();
			else if (index == 3) $scope.oModal3.show();
			else if (index == 4) $scope.oModal4.show();
			else if (index == 5) $scope.oModal5.show();
			else if (index == 6) $scope.oModal6.show();
		};
		$scope.closeModal = function (index) {
			if (index == 1) $scope.oModal1.hide();
			else if (index == 2) $scope.oModal2.hide();
			else if (index == 3) $scope.oModal3.hide();
			else if (index == 4) $scope.oModal4.hide();
			else if (index == 5) $scope.oModal5.hide();
			else if (index == 6) $scope.oModal6.hide();
		};
		// Cleanup the modal when we're done with it!
		$scope.$on('$destroy', function () {
			console.log('Destroying modals...');
			$scope.oModal1.remove();
			$scope.oModal2.remove();
			$scope.oModal3.remove();
			$scope.oModal4.remove();
			$scope.oModal5.remove();
			$scope.oModal6.remove();
		}); //modal end here

		$scope.itemSelction = function (id, arr) {
			arr.map(function (obj) {
				if (obj.id === id) {
					obj.selected = true;
				} else {
					obj.selected = false;
				}
			});
			console.log('updated arr is ', arr);
			var selectedItem = arr.find(function (obj) {
				if (obj.selected === true) {
					return obj;
				}
			});
			// console.log('selected item is ', selectedItem);
			return selectedItem;
		};
		// product selection
		$scope.select_item = function (id) {
			$scope.selectedBottle = $scope.itemSelction(id, $scope.user.bottles); //bottle array is passed for itemSelction
			console.log('selected product is ', $scope.selectedBottle);
			
		};

		$scope.$on('modal.shown', function (event, modal) {
			console.log('Modal ' + modal.id + ' is shown!');
			if (modal.id === '3') {
				$scope.select_item($scope.user.bottles[0].id);

			}
		});
		//slider
		$scope.options = { autoHeight: true }
		$scope.data = {
			isLoading: false
		};//slider ends here

		$scope.showReport = function (idx, reportURL) {
			$scope.imageReport = reportURL;
			$scope.openModal(idx);
		};

		$scope.saveProfile = function (form) {
			if (form.$valid) {
				console.log('usr data are ', $scope.usr);
				$scope.data = {
					first_name: $scope.usr.firstname,
					last_name: $scope.usr.lastname,
					email: $scope.usr.email,
				};
				$ionicLoading.show({
					template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Saving profile</p>',
					animation: 'fade-in',
					showBackdrop: true,
					showDelay: 0
				});
				UserService.profile($scope.data).then(successCallback, errorCallback);
				function successCallback(result) {
					console.log('user profile success callback', result);
					$ionicLoading.hide();
					if (result.data.success) {
						console.log('user profile success true', result);
						//$scope.user = $rootScope.prepareUserData(result.data.response.bottles);
						$scope.user = $rootScope.prepareUserData(result.data.response);
						$scope.headline = $scope.user.firstname + ' ' + $scope.user.lastname;
						window.plugins.toast.showShortTop(
							"Profile saved!", function (a) { }, function (b) { }
						);
					} else {
						console.log('user profile success false', result);
					}
				};
				function errorCallback(response) {
					$ionicLoading.hide();
					console.log('user profile errorCallback', response);
				};
			} else {
				console.log("Form is invalid");
				$ionicLoading.hide();
				window.plugins.toast.showLongTop(
					"Please provide details", function (a) { }, function (b) { }
				);
			}
		};

		// $scope.refundBottles = function (id) {
		// 	var bottle = id;
		// 	if (+$scope.user.walletBottles) {
		// 		var bottlePopUp = $ionicPopup.show({
		// 			title: 'Refund Bottles?',
		// 			scope: $scope,
		// 			buttons: [{
		// 				text: 'No',
		// 				type: 'button-later',
		// 				onTap: function (e) {
		// 					bottlePopUp.close();
		// 				}
		// 			},
		// 			{
		// 				text: '<b>Yes</b>',
		// 				type: 'button-get-now',
		// 				onTap: function (e) {
		// 					$ionicLoading.show({
		// 						template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Refund request</p>',
		// 						animation: 'fade-in',
		// 						showBackdrop: true,
		// 						showDelay: 0
		// 					});
		// 					Order.refundBottles({ bottleId: bottle }).then(refundSuccess, refundError);
		// 					function refundSuccess(res) {
		// 						console.log('refund bottle res', res);
		// 						$ionicLoading.hide();
		// 						if (res.success) {
		// 							$scope.user = $rootScope.prepareUserData(res.response.customer);
		// 							window.plugins.toast.showShortTop(
		// 								"Bottles Refund requested!", function (a) { }, function (b) { }
		// 							);
		// 						} else {
		// 							console.log('something looks bad', res);
		// 							// $ionicLoading.hide();
		// 						}
		// 					}
		// 					function refundError(res) {
		// 						console.log("something is not good n it's error", res);
		// 						$ionicLoading.hide();
		// 					};
		// 				}
		// 			}
		// 			]
		// 		});
		// 	} else {
		// 		window.plugins.toast.showShortTop(
		// 			"Bottle Balance is ZERO", function (a) { }, function (b) { }
		// 		);

		// 	}
		// };
		// $scope.cancelBottleRefund = function (id) {
		// 	$ionicLoading.show({
		// 		template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Adding Bottles to Wallet</p>',
		// 		animation: 'fade-in',
		// 		showBackdrop: true,
		// 		showDelay: 0
		// 	});
		// 	Order.cancelBottleRefund({ bottleId: id }).then(refundBottleSucces, refundBottleFailure);
		// 	function refundBottleSucces(res) {
		// 		console.log('bottle refund cancellation', res);
		// 		$ionicLoading.hide();
		// 		if (res.success) { //need to edit the customer image url
		// 			$scope.user = $rootScope.prepareUserData(res.response.customer);
		// 			window.plugins.toast.showShortTop(
		// 				"Bottles are added back to your Wallet", function (a) { }, function (b) { }
		// 			);
		// 		}
		// 	};
		// 	function refundBottleFailure(res) {
		// 		$ionicLoading.hide();
		// 	};
		// };

		$scope.depositRefund = function (id) {

			//if (!$scope.user.isRefund) {
				var cancelPopUp = $ionicPopup.show({
					title: 'Refund Deposit? This action will lock your account!',
					scope: $scope,
					buttons: [{
						text: 'No',
						type: 'button-later',
						onTap: function (e) {
							cancelPopUp.close();
						}
					},
					{
						text: '<b>Yes</b>',
						type: 'button-assertive',
						onTap: function (e) {
							$ionicLoading.show({
								template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Deposit Refund</p>',
								animation: 'fade-in',
								showBackdrop: true,
								showDelay: 0
							});
							//send bottle id
							Order.refundBottles({ bottleId: id }).then(refundSuccess, refundError);
							function refundSuccess(res) {
								$ionicLoading.hide();
								console.log('Deposit refund promise is resolved ', res);
								if (res.success) {
									$scope.refundID = res.response.refundId;
									$scope.user = $rootScope.prepareUserData(res.response.customer);
									$scope.select_item($scope.user.bottles[0].id);
									window.plugins.toast.showLongBottom(
										"Deposit Refund requested. Our executive will contact you shortly. Thank you!", function (a) { }, function (b) { }
									);
								} else {
									console.log('something looks bad', res);
									// $ionicLoading.hide();
								}
							}
							function refundError(res) {
								console.log("something is not good n it's error", res);
								$ionicLoading.hide();
							};
						}
					}
					]
				});
			//}
			// if (!$scope.user.depositAmt) {
			// 	window.plugins.toast.showShortBottom(
			// 		"You have not paid deposit", function (a) { }, function (b) { }
			// 	);
			// }
			// if ($scope.user.isRefund) {
			// 	window.plugins.toast.showShortBottom(
			// 		"Deposit Refund is under process", function (a) { }, function (b) { }
			// 	);
			// }

		};
		$scope.cancelRefund = function (id) {
			$ionicLoading.show({
				template: '<ion-spinner icon="ripple" class="spinner-calm"></ion-spinner><p>Unlocking</p>',
				animation: 'fade-in',
				showBackdrop: true,
				showDelay: 0
			});
			Order.cancelBottleRefund({ bottleId: id }).then(cancelSucces, cancelError);
			function cancelSucces(res) {
				$ionicLoading.hide();
				console.log("cancelRefund promise resolved", res);
				if (res.success) {
					$scope.user = $rootScope.prepareUserData(res.response.customer);
					$scope.select_item($scope.user.bottles[0].id);
					window.plugins.toast.showShortBottom(
						"Enjoy the service!", function (a) { }, function (b) { }
					);
				} else {
					console.log("cancelRefund promise gone wrong", res);
					$ionicLoading.hide();
				}
			};
			function cancelError(error) {
				console.log("cancelRefund promise is rejected", error);
			};
		};

		$scope.logout = function () {
			var logOutPopUp = $ionicPopup.show({
				title: 'Do you want to log out?',
				scope: $scope,
				buttons: [{
					text: 'Nope',
					type: 'button-later',
					onTap: function (e) {
						logOutPopUp.close();
					}
				},
				{
					text: '<b>logout</b>',
					type: 'button-get-now',
					onTap: function (e) {
						window.localStorage.clear();
						ionic.Platform.exitApp();
					}
				}
				]
			});

		};

		$scope.queries = [
			{
				question: "What is Water Wallet?",
				answer: 'Water Wallet is digital way of storing your drinking water in the cloud. Through Water Wallet you earn savings and most importantly you can enjoy the Auto Delivery feature.',
				show: false
			},
			{
				question: "Does the Bottle Delivery be placed after adding bottles to the Wallet?",
				answer: 'No. Bottles are added to the Wallet doesn’t place for delivery. You need to place the delivery for water by clicking “Get Delivery Now” on Home page. Or you can set the Auto Delivery for your Wallet Bottles.',
				show: false
			},
			{
				question: "What is Auto Delivery?",
				answer: 'Auto Delivery is a process of automating water delivery request without your intervention. You set the frequency of delivery only once and the delivery happens until the Bottles in the Wallet are exhausted.',
				show: false
			},
			{
				question: "How does Auto Delivery work?",
				answer: 'Auto Delivery works only if you’ve Bottles in your Wallet. Auto Delivery will schedule your upcoming delivery date and time. You can alter the upcoming delivery any given time except after the Auto Delivery is placed for the day. If the Auto Delivery is canceled by you or by Delivery Boy the delivery of such bottle will be automatically attempted the next day. Auto Delivery works like charm to keep you hydrated.',
				show: false
			},
			{
				question: "Can I get water delivery without using Water Wallet feature?",
				answer: 'Yes. You can place an order for Water without Wallet involved. Just click on Get Delivery Now on Home page to place a delivery for Water Can without any hassles.',
				show: false
			},
			{
				question: "I use Auto Delivery feature and I want reschedule my upcoming delivery. Is it possible?",
				answer: 'Yes. You can reschedule the Auto Delivery date and time before the order is placed for the day. Once the order is placed you can only cancel the delivery and reschedule. If the Auto Delivery is canceled by you or by Delivery Boy the delivery of such bottle will be automatically attempted the next day.',
				show: false
			},
			{
				question: "How can I track my in transit delivery?",
				answer: 'You track the in transit deliveries by visiting the Order page  Deliveries  Click on the Placed Delivery card which usually appears on top(sometimes wait for few sec to load the deliveries).',
				show: false
			},
			{
				question: "What are Transactions and Deliveries in Order page?",
				answer: 'Transactions are purchases you made on EazyBottle such as Wallet Purchases and etc. Deliveries are the bottle delivery information that is Placed, Delivered or Cancelled state.',
				show: false
			},
			{
				question: "I’m moving out of town and I need my deposit back. How to get it?",
				answer: 'During the times of relocation to the area where the EazyBottle service is not available you can raise the Refund request at your comforts. Go to Account  My Account  Refund Request will raise the request. After the request EazyBottle executive will visit to your delivery address to collect the bottle and then initiate the Refund money.',
				show: false
			},
			{
				question: "Will I get refund of my Wallet Bottles?",
				answer: 'Yes. You’re eligible to get your Wallet Bottles refund for the bottles you have purchased. Any bottles that are given as FREE from EazyBottle are not eligible for such refunds.',
				show: false
			}
		];

		/*
		 * if given group is the selected group, deselect it
		 * else, select the given group
		 */
		$scope.toggleQuery = function (query) {
			query.show = !query.show;
		};
		$scope.isQueryShown = function (query) {
			return query.show;
		};

	})