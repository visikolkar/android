
function scheduleDeliveryNotification(slotid, repeat, delivery_id, usr) {
    if(hasPermission() == 'No') {
        registerPermission = function () {
                cordova.plugins.notification.local.registerPermission(function (granted) {
                    addNotification(slotid, repeat, delivery_id, usr);
                });
            };
    }
    else {
        addNotification(slotid, repeat, delivery_id, usr);
    }
}

function cancelDeliveryNotification(noti_id) {
    cordova.plugins.notification.local.cancel(noti_id, function(){
        console.log("Notification has been cancelled");
    });
}

function hasPermission () {
    cordova.plugins.notification.local.hasPermission(function (granted) {
        console.log(granted ? 'Yes' : 'No');
    });
}

function addNotification(slotid, repeat, delivery_id, usr) {
    
    var noti_time = new Date();
    var delivery_time = noti_time;

    if(slotid == 1 && noti_time.getHours() < 6)//if 8~9 slot is selected and time of order is before 6. dont show notification
    {
        return ;
    }
    else if(slotid == 1 && noti_time.getHours() > 20) //if 8~9 slot is selected and time of order is after 8PM. Dont show any notification
    {
        return ;
    }
    else if(slotid == 1)
    {
        noti_time.setHours(20);//Show notification one day before at 8PMs
        delivery_time.setDate(noti_time.getDate()+24*60*60*1000);//tomorrow
        delivery_time.setHours(8);
    }
    else if((noti_time.getHours() == 18 && noti_time.getMinutes() > 30) || noti_time.getHours() > 18) //slot is for tomorrow (closure time : 6.30)
    {
        noti_time.setDate(noti_time.getDate()+24*60*60*1000);//tomorrow
        noti_time.setHours(6+slotid);//6+2=8 -> notification time for slot 9-10AM

        delivery_time = noti_time;
        delivery_time.setHours(7+slotid);
    }
    else //slot is for today
    {
        noti_time.setHours(6+slotid);//6+2=8 -> notification time for slot 9-10AM

        delivery_time = noti_time;
        delivery_time.setHours(7+slotid);
    }

    noti_time.setMinutes(0);
    noti_time.setSeconds(0);    

    //testing - show notification after a minute from now
    noti_time = new Date();
    //noti_time.setMinutes(noti_time.getMinutes()+3);
    noti_time.setSeconds(noti_time.getSeconds()+10);
    
    cordova.plugins.notification.local.schedule({   id: delivery_id,
                                                    title: usr,
                                                    text: "You have an upcoming delivery",
                                                    at: noti_time.getTime(),
                                                    every: repeat,
                                                    sound: "file://sounds/delivery_noti.wma",
                                                    image: "file://img/icon.png",
                                                    data:{'deliveryId':delivery_id, 'deliveryTime':delivery_time.getTime()}
                                                });

    //cordova.plugins.notification.local.on("click", callback);
}

function getDeliveryTimeStr(deliveryTime) {
    var today = new Date();
    deliveryTime = new Date(deliveryTime);
    var timeStr;

    if(today.getDay() == deliveryTime.getDay())
        timeStr = "today at ";
    else 
        timeStr = "tomorrow at ";

    if(deliveryTime.getHours() > 12)
        timeStr += deliveryTime.getHours() -12 + deliveryTime.getMinutes() + 'PM';
    else
        timeStr += deliveryTime.getHours() + deliveryTime.getMinutes() + 'AM';
    
    return timeStr;
}

/*
Notification will not work under below conditions:
1. Auto start disabled -> app is killed by user
2. Auto start disabled -> device reboots
*/

/* Test scenarios
1. Place an order -> minimize app -> notification should be shown at correct time
2. Place an order -> kill app -> notification should be shown at correct time
3. Place an order -> lock the device -> notification should be shown at correct time
4. Place an order -> reboot the device -> notification should be shown at correct time

NOTE: Check all the above scenarios with auto start disabled
*/

/* TODO
1. Get time from delivery deliveryId
2. Postpone
3. Snooze notification untill user takes Action
4. Request for auto start
*/