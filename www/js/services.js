angular.module('eazybottle')

	.factory('Popup', function ($ionicPopup) {
		return {
			showAlert: function (sub) {
				var alertPopup = $ionicPopup.alert({
					title: sub,
					cssClass: 'button-later' //need to test this
				});
				return alertPopup;
			}
		};
	})

	.factory('Login', function ($http, SERVER) {
		return {
			sendOTP: function (phoneNum) {
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/user/signup-phone',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: phoneNum
				};
				return $http(req);
			},
			validateOTP: function (login) {
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/user/verify-phone',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: login
				};
				return $http(req);
			},
			reSendOTP: function (phoneNum) {
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/user/generateOTP',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: phoneNum
				};
				return $http(req);
			},
		};

	})

	.service("localnotificationservice", function() {
		return true;
	})
	.service('UserService', function ($http, $q, SERVER) {

		var deferred = $q.defer();

		this.getInfo = function () {
			return $http.get(SERVER.URL + '/EazyBottle/app/rest/customer/info', { cache: false });//{timeout: 30000, cache: false}
			// .then(function (res) {
			// 	deferred.resolve(res.data);
			// 	return deferred.promise;
			// }, function (res) {
			// 	deferred.reject(res);
			// 	return deferred.promise;
			// });
		};

		this.totalBottles = function (arr) {
			var result = [];
			//var bottlesLength = arr.length;
			// for (var i = 0; i < bottlesLength; i++) {
			// 	if (arr[i].refundStatus == 'NONE') {
			// 		result.push(arr[i].paid + arr[i].free);
			// 	}
			// }
			arr.forEach(function (obj) {
				if (obj.refundStatus == 'NONE') { //check if the bottles are requested for refund
					result.push(obj.paid + obj.free);
				}
			});
			var bottles = result.reduce(function (a, b) {
				return a + b;
			}, 0);
			if (bottles.toString().length == 1) {
				return '0' + bottles
			} else {
				return bottles;
			}
		};

		this.storeInfo = function (data) {
			window.localStorage.userInfo = JSON.stringify(data);
		};

		this.fetchUserInfo = function () {
			if (!window.localStorage.userInfo) {
				return window.localStorage.userInfo;
			} else {
				return JSON.parse(window.localStorage.userInfo);
			}
		};

		this.serverTime = function () {
			return $http.get(SERVER.URL + '/EazyBottle/app/user/time', { cache: false });
		};

		this.profile = function(usr){
			var req = {
                method: 'POST',
                url: SERVER.URL + '/EazyBottle/app/rest/customer/update_profile',
                headers: {
                    'Content-Type': 'application/json'
                },
                params: usr
            };
            return $http(req);
		};
	})

	.factory('Vendors', function ($http, $q, SERVER) {
		return {
			getVendors: function () {
				var deferred = $q.defer();
				$http.get(SERVER.URL + '/EazyBottle/app/rest/vendor/list')
					.then(function (res) {
						deferred.resolve(res.data);
					}, function (res) {
						deferred.reject(res);
					});
				return deferred.promise;
			},
			storeVendors: function (data) {
				window.localStorage.vendors = JSON.stringify(data);
			},
			fetchVendors: function () {
				if (!window.localStorage.vendors) {
					return window.localStorage.vendors;
				} else {
					// var result =[];
					// var vendorArray = JSON.parse(window.localStorage.vendors);
					// vendorArray.forEach(function(obj){
					// 	result.push(obj.vendor);
					// });
					return JSON.parse(window.localStorage.vendors);
					// return result;
				}
			},
		};
	})

	.factory('Products', function ($http, $q, SERVER) {
		return {
			getProducts: function () {
				var deferred = $q.defer();
				$http.get(SERVER.URL + '/EazyBottle/app/rest/product/list')
					.then(function (res) {
						deferred.resolve(res.data);
					}, function (res) {
						deferred.reject(res);
					});
				return deferred.promise;
			},
			storeProducts: function (data) {
				window.localStorage.products = JSON.stringify(data);
			},
			fetchProducts: function () {
				if (!window.localStorage.products) {
					return window.localStorage.products;
				} else {
					return JSON.parse(window.localStorage.products);
				}
			},
		};
	})

	.factory('Slots', function ($http, $q, SERVER) {
		var slots;
		function tConvert(time) {
			time = time.toString().match(/^([01]\d|2[0-3])(:)([0-5]\d)(:[0-5]\d)?$/) || [time];
			console.log('time is ', time);

			if (time.length > 1) {
				time = time.slice(1);
				time[3] = +time[0] < 12 ? 'AM' : 'PM';
				console.log('time with AM or PM ', time);
				time.splice(1, 1);
				console.log('time without : ', time);
				time[0] = +time[0] % 12 || 12;
			}
			return time.join(':');
		};
		return {
			getSlotsList: function () {
				var deferred = $q.defer();
				$http.get(SERVER.URL + '/EazyBottle/app/rest/slot/list')
					.then(function (res) {
						deferred.resolve(res.data);
					}, function (res) {
						deferred.reject(res);
					});
				return deferred.promise;
			},
			storeSlots: function (data) {
				// slots = data;
				window.localStorage.slots = JSON.stringify(data);;
			},
			fetchSlots: function () {
				if (!window.localStorage.slots) {
					return window.localStorage.slots;
				} else {
					return JSON.parse(window.localStorage.slots);
				}
			},
			getTimelySlots: function () {
				var self = this;
				// var data = JSON.parse(window.localStorage.slots);
				var data = self.fetchSlots();
				var date = new Date();
				var hour = date.getHours();
				var minutes = date.getMinutes();
				var h = '';
				if (hour.toString().length == 1) {
					hour = "0" + hour;
				}
				if (minutes.toString().length == 1) {
					minutes = "0" + minutes;
				}
				var time = tConvert(hour + ':' + minutes);
				console.log('time is ', time);
				if (hour >= 19 || (hour == 18 && minutes >= 30) || hour <= 5) {
					return data;
				} else {
					var time = tConvert(hour + ':' + minutes);
					console.log('time is ', time);
					// this condition for 831 931 1031 131 etc...
					if (+time.split(':')[1] > 30) {
						if (time.split(':')[0] + time.split(':')[2] === '10AM' || time.split(':')[0] + time.split(':')[2] === '11AM' || time.split(':')[0] + time.split(':')[2] === '12PM') {
							h = '3PM';
						} else {
							h = +time.split(':')[0] + 3;
							if (h == 12) {
								h = h + 'PM'
							} 
							else{
								h += time.split(':')[2];
							}
						}
						console.log('the >30 minutes h is ' + h);
					} else { // 815 915 1015 etc...
						if (time.split(':')[0] + time.split(':')[2] === '11AM' || time.split(':')[0] + time.split(':')[2] === '12PM') {
							h = '3PM';
						} else {
							h = +time.split(':')[0] + 2;
							if (h == 12) {
								h = h + 'PM'
							} else {
								h = h + time.split(':')[2];
							}
						}
						console.log('the <30 minutes h is ' + h);
					}
					//var data = [{id: 1, time: '9AM-10AM'}, {id: 2, time: '10AM-11AM'}, {id: 3, time: '11AM-12PM'}, {id: 4, time: '12PM-1PM'}, {id: 5, time: '3PM-4PM'}, {id: 6, time: '4PM-5PM'}, {id: 7, time: '5PM-6PM'}, {id: 8, time: '6PM-7PM'}, {id: 9, time: '7PM-8PM'}, {id: 10, time: '8PM-9PM'}]
					var findv = data.find(function (value) {
						return value.from === h;
					});

					console.log('returned object is', findv);

					var available = data.filter(function (num) {
						if (num.id >= findv.id) {
							return {
								id: num.id,
								time: num.time
							};
						}
					});
					return available;
					// var result = [];
					// for (var i = 0; i < available.length; i++) {
					// 	if (typeof available[i] !== 'undefined') {
					// 		result.push(available[i]);
					// 	}
					// }
					// console.log('getTimelySlots result arr', result);
					// return result;

				}
			},
			setAutoDays: function (info) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/customer/set_recurring_days',
					params: info
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			setAutoSlot: function (info) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/customer/set_recurring_slot',
					params: info
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			setAutoDelivery: function (isAuto) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/customer/set_auto_delivery',
					params: isAuto
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			setAutoUpcomingDelivery: function (date) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/customer/set_auto_delivery_date',
					params: date
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			}
		};

	})

	.factory('Address', function ($http, $q, SERVER) {
		var updateFlag = false;
		return {
			saveAddress: function (address) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/address/insert',
					data: address
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			setUpdateFlag: function (boolean) {
				updateFlag = boolean;
			},
			getUpdateFlag: function () {
				return updateFlag;
			},
			getUpdateIndex: function () {
				return updateIndex;
			},
			updateAddress: function (address) {
				var deferred = $q.defer()
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/address/update',
					data: address
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			checkUserPoint: function (point, polygons) {

				// ray-casting algorithm based on
				// http://www.ecse.rpi.edu/Homepages/wrf/Research/Short_Notes/pnpoly.html

				// polygon = [[12.92179, 77.66083],[12.91699, 77.67384],[12.91763, 77.67617],[12.92167, 77.67822],[12.9254, 77.67978],[12.9276, 77.68047],[12.92904, 77.67996],[12.934, 77.67802],[12.93084, 77.66569]];
				var self = this;
				for (p = 0; p < polygons.length; p++) {
					var polygon = polygons[p];
					if (self.isPointInside(point, polygon)) {
						return true;
					}
				}
				return false;

			},
			isPointInside: function (point, polygon) {
				var x = point[0], y = point[1];
				var inside = false;
				console.log('polygon length is ', polygon.length);
				for (var i = 0, j = polygon.length - 1; i < polygon.length; j = i++) {
					var xi = polygon[i][0], yi = polygon[i][1];
					var xj = polygon[j][0], yj = polygon[j][1];

					var intersect = ((yi > y) != (yj > y))
						&& (x < (xj - xi) * (y - yi) / (yj - yi) + xi);
					if (intersect) inside = !inside;
				}

				return inside;
			},
			businessAreaList: function () {
				var deferred = $q.defer();
				$http.get(SERVER.URL + '/EazyBottle/app/rest/businessarea/list')
					.then(function (res) {
						deferred.resolve(res.data);
					}, function (res) {
						deferred.reject(res);
					});
				return deferred.promise;
			},
			storeBusinessArea: function (data) {
				window.localStorage.areas = JSON.stringify(data);
			},
			fetchBusinessArea: function () {
				if (!window.localStorage.areas) {
					return window.localStorage.areas;
				} else {
					return JSON.parse(window.localStorage.areas);
				}
			},
			businessValues: function() {
				var deferred = $q.defer();
				$http.get(SERVER.URL + '/EazyBottle/app/rest/business/values')
					.then(function (res) {
						deferred.resolve(res.data);
					}, function (res) {
						deferred.reject(res);
					});
				return deferred.promise;
			},
			storeBusinessValues: function (data) {
				window.localStorage.values = JSON.stringify(data);
			},
			fetchBusinessValues: function () {
				if (!window.localStorage.values) {
					return window.localStorage.values;
				} else {
					return JSON.parse(window.localStorage.values);
				}
			},
		};
	})

	.factory('Order', function ($http, $q, SERVER) {
		return {
			getDelivery: function () {
				var deferred = $q.defer();
				var url = SERVER.URL + '/EazyBottle/app/rest/delivery/list';
				$http.get(url).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			storeDeliveryList: function (data) {
				window.localStorage.deliveryList = JSON.stringify(data);
			},
			fetchDeliveryList: function () {
				if (!window.localStorage.deliveryList) {
					return window.localStorage.deliveryList;
				} else {
					return JSON.parse(window.localStorage.deliveryList);
				}
			},
			getOrders: function () {
				var deferred = $q.defer();
				var url = SERVER.URL + '/EazyBottle/app/rest/order/list';
				$http.get(url).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			storeOrderList: function (data) {
				window.localStorage.orderList = JSON.stringify(data);
			},
			fetchOrderList: function () {
				if (!window.localStorage.orderList) {
					return window.localStorage.orderList;
				} else {
					return JSON.parse(window.localStorage.orderList);
				}
			},
			getDeliveryDetail: function (delId) {
				console.log('delivery details requested for ', delId);
				var arr = JSON.parse(window.localStorage.deliveryList);
				for (var i = 0; i < arr.length; i++) {
					if (arr[i].id == parseInt(delId)) {
						return arr[i];
					}
				}
				return null;
			},
			refundBottles: function (bottle) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/refund_request/bottles',
					params: bottle
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			cancelBottleRefund: function (bottle) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/refund_request/cancelBottleRefund',
					params: bottle
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			refundReq: function () {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/refund_request/deposit'
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			refundCancel: function () {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/refund_request/cancel'
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			cancelDelivery: function (id) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/delivery/cancel',
					params: id
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			postponeDelivery: function (id) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/delivery/postpone',
					params: id
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
		}
	})

	.factory('Payment', function ($http, $q, SERVER) {
		return {
			placeOrder: function (order) {
				var deferred = $q.defer();
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/order/place',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: order
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			placeDelivery: function (order) {
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/delivery/place',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: order
				};
				$http(req).then(function (res) {
					deferred.resolve(res.data);
				}, function (res) {
					deferred.reject(res);
				});
				return deferred.promise;
			},
			addBottles: function (order) {
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/order/addbottles',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: order
				};
				return $http(req);
			},
			paymentFailure: function (order) {
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/rest/order/fail',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: order
				};
				return $http(req);
			},
			generateChecksum: function(obj){
				//https://eazybottle.com/EazyBottle/app/paytm/generateChecksum
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/paytm/generateChecksum',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: obj
				};
				return $http(req);
			},
			reVerify: function(obj){
				var req = {
					method: 'POST',
					url: SERVER.URL + '/EazyBottle/app/paytm/callback2',
					headers: {
						'Content-Type': 'application/x-www-form-urlencoded; charset=utf-8'
					},
					params: obj
				};
				console.log('reVerify api call object', req);
				return $http(req);
			}
		};
	})

	.factory('Auth', function () {
		return {
			token: function (response) {
				window.localStorage.access_token = response.access_token;
				window.localStorage.refresh_token = response.refresh_token;
				window.localStorage.expires_in = response.expires_in;
				// var expireDate = new Date(new Date().getTime() + (1000 * response.expires_in));
			},
			getAccessToken: function () {
				return window.localStorage.access_token;
			},
			getRefreshToken: function () {
				return window.localStorage.refresh_token;
			},
			clear: function () {
				window.localStorage.clear();
			},
			newAccessToken: function () {
				var self = this;
				var data = {
					grant_type: 'refresh_token',
					client_id: 'my-trusted-client',
					refresh_token: self.getRefreshToken()
				};
				return data;
			},
			isLoggedIn: function () {
				if (window.localStorage.refresh_token) {
					return true;
				} else {
					return false;
				}
			},
		};
	})

	.factory('AuthInterceptor', function ($q, $injector, Auth, SERVER) {
		var inFlightAuthRequest = null;
		var count = 1;
		return {
			request: function (config) {
				if (Auth.getAccessToken()) {
					config.headers.Authorization = 'Bearer ' + Auth.getAccessToken();
				}
				return config;
			},
			responseError: function (response) {
				console.log("interceptor responseError", response);
				switch (response.status) {
					case 401:
						// if(response.data.error !== 'unauthorized' && response.data.error_description !== null){
						if (response.data.error === 'invalid_token' && (response.data.error_description.split(':')[0] === 'Access token expired' || response.data.error_description.split(':')[0] === 'Invalid access token')) {
							console.log("inside the Access Token expired condition");
							var deferred = $q.defer();
							if (!inFlightAuthRequest) {
								console.log("inside the inFlightAuthRequest condition requesting tokens");
								inflightAuthRequest = $injector.get("$http").post(SERVER.URL + '/EazyBottle/app/oauth/token/?grant_type=refresh_token&client_id=my-trusted-client&refresh_token=' + Auth.getRefreshToken());
							}
							// var inflightAuthRequest = $injector.get("$http").post('/api/oauth/', { params: Auth.newAccessToken() });
							inflightAuthRequest.then(function (r) {
								inflightAuthRequest = null;
								console.log("inside the inFlightAuthRequest.then api call");
								if (typeof r !== 'undefined' && r.data.access_token && r.data.refresh_token) {
									Auth.token(r.data);
									console.log('requested auth tokens ', r);
									console.log('failed apis config', response);
									$injector.get("$http")(response.config).then(function (resp) {
										deferred.resolve(resp);
									}, function (resp) {
										deferred.reject();
									});
								} else {
									deferred.reject();
								}
							}, function (response) {
								inflightAuthRequest = null;
								deferred.reject();
								Auth.clear();
								$injector.get("$state").go('login');
								// return;
							});
							return deferred.promise;
						} else {
							inflightAuthRequest = null;
							Auth.clear();
							$injector.get("$state").go('login');
							// return;
						}
						break;
					case 500:
						// retry after the 500 error
						console.log("inside the 500 error case");
						var deferred = $q.defer();
						// var count = 1;
						console.log("inside the 500 error case count value " + count);
						if (count < 4) {
							count++;
							$injector.get("$http")(response.config).then(function (resp) {
								deferred.resolve(resp);
							}, function (resp) {
								deferred.reject();
							});
							return deferred.promise;
						} else {
							// window.plugins.toast.showShortBottom(
							// 	"Something doesn't look good", function (a) { }, function (b) { }
							// );
							console.log('500 case reached limit condition');
						}
						break;
					case 404:
						window.plugins.toast.showShortBottom(
							"Server Page not found", function (a) { }, function (b) { }
						);
						break;
					case 400:
						if (response.data.error_description === 'Bad credentials') {
							window.plugins.toast.showShortBottom(
								"Invalid Credentials", function (a) { }, function (b) { }
							);
						} 
						// else {
						// 	window.plugins.toast.showShortBottom(
						// 		"Bad Request", function (a) { }, function (b) { }
						// 	);
						// }
						if(response.data.error === "invalid_grant"){
							inflightAuthRequest = null;
							Auth.clear();
							$injector.get("$state").go('login');
						}
						break;
					case -1:
						window.plugins.toast.showShortBottom(
							"Internet Connection Problem", function (a) { }, function (b) { }
						);
						break;
					default:
						Auth.clear();
						$injector.get("$state").go('login');
						break;
				}
				return response || $q.when(response);
			}
		};
	})

	.config(function ($httpProvider) {
		$httpProvider.interceptors.push('AuthInterceptor');
	})